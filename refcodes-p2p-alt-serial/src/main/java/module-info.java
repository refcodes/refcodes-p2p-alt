module org.refcodes.p2p.alt.serial {
	requires transitive org.refcodes.p2p;
	requires transitive org.refcodes.serial;
	requires transitive org.refcodes.serial.ext.handshake;
	requires java.logging;

	exports org.refcodes.p2p.alt.serial;
}
