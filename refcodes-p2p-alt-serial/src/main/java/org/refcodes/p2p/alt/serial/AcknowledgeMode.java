package org.refcodes.p2p.alt.serial;

import org.refcodes.mixin.EnabledAccessor;

/**
 * The {@link AcknowledgeMode} determines the mode of operation regarding
 * acknowledging hop count and send message transmissions.
 */
public enum AcknowledgeMode implements EnabledAccessor {
	/**
	 * Enable acknowledgement transmissions and requests.
	 */
	ON(true),

	/**
	 * Disable acknowledgement transmissions and requests.
	 */
	OFF(false);

	private boolean _isEnabled;

	private AcknowledgeMode( boolean isEnabled ) {
		_isEnabled = isEnabled;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isEnabled() {
		return _isEnabled;
	}
}