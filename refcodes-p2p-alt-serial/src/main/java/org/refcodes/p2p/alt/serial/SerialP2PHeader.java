// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.p2p.alt.serial;

import static org.refcodes.serial.SerialSugar.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.refcodes.mixin.MagicBytesAccessor;
import org.refcodes.mixin.MagicNumberAccessor;
import org.refcodes.p2p.AbstractP2PHeader;
import org.refcodes.p2p.P2PHeader;
import org.refcodes.serial.AssertMagicBytesSegment;
import org.refcodes.serial.IntSegment;
import org.refcodes.serial.Segment;
import org.refcodes.serial.Segment.SegmentMixin;
import org.refcodes.serial.Sequence;
import org.refcodes.serial.SerialSchema;
import org.refcodes.serial.TransmissionException;
import org.refcodes.serial.TransmissionMetrics;
import org.refcodes.struct.SimpleTypeMap;

/**
 * The {@link SerialP2PHeader} represents a {@link P2PHeader} optimized for
 * local microcontroller wiring (networks) and microcontroller memory layout:
 * The visited hops are serialized and deserialized in the tail of the
 * {@link SerialP2PMessage} as this makes append the last hop easier for devices
 * with small memory buffers.
 */
public class SerialP2PHeader extends AbstractP2PHeader<Integer> implements MagicNumberAccessor<Integer>, MagicBytesAccessor, Segment, SegmentMixin {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final SerialP2PTransmissionMetrics DEFAULT_TRANSMISSION_METRICS = new SerialP2PTransmissionMetrics();

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private Segment _delegatee;
	private AssertMagicBytesSegment _magicBytesSegment;
	private IntSegment _destinationSegment;
	private IntSegment _magicNumberSegment;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the header using the given destination locator as destination
	 * ({@link #getDestination()}). The source is added upon transmission as
	 * first stop-over.
	 * 
	 * @param aDestination The destination locator to use.
	 */
	SerialP2PHeader( Integer aDestination ) {
		this( -1, aDestination, DEFAULT_TRANSMISSION_METRICS );
	}

	/**
	 * Constructs the header using the given destination locator as destination
	 * ({@link #getDestination()}). The source is added upon transmission as
	 * first stop-over.
	 * 
	 * @param aMagicNumber The magic number for tagging this message when being
	 *        transmitted.
	 * @param aDestination The destination locator to use.
	 * @param aTransmissionMetrics the a {@link TransmissionMetrics} to use.
	 */
	SerialP2PHeader( Integer aMagicNumber, Integer aDestination, SerialP2PTransmissionMetrics aTransmissionMetrics ) {
		super( aDestination );
		aTransmissionMetrics = aTransmissionMetrics != null ? aTransmissionMetrics : DEFAULT_TRANSMISSION_METRICS;
		// @formatter:off
		_delegatee = segmentComposite(  
			crcSegment(
				segmentComposite(
					_magicBytesSegment = assertMagicBytesSegment( aTransmissionMetrics.getP2PMessageMagicBytes(), aTransmissionMetrics ),
					_magicNumberSegment = intSegment( aMagicNumber, aTransmissionMetrics ),
					_destinationSegment = intSegment(aDestination, aTransmissionMetrics )
				), 
				aTransmissionMetrics
			)
		);
		// @formatter:on
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] getMagicBytes() {
		return _magicBytesSegment.getMagicBytes();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Integer getMagicNumber() {
		return _magicNumberSegment.getPayload();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getLength() {
		return _delegatee.getLength();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() { /* this is an immutable type */ }

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence toSequence() {
		_destinationSegment.setPayload( _destination );
		return _delegatee.toSequence();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void transmitTo( OutputStream aOutputStream, InputStream aReturnStream ) throws IOException {
		_destinationSegment.setPayload( _destination );
		_delegatee.transmitTo( aOutputStream, aReturnStream );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SerialSchema toSchema() {
		return _delegatee.toSchema();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SimpleTypeMap toSimpleTypeMap() {
		return _delegatee.toSimpleTypeMap();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int fromTransmission( Sequence aSequence, int aOffset ) throws TransmissionException {
		final int theOffset = _delegatee.fromTransmission( aSequence, aOffset );
		_destination = _destinationSegment.getPayload();
		return theOffset;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void receiveFrom( InputStream aInputStream, OutputStream aReturnStream ) throws IOException {
		_delegatee.receiveFrom( aInputStream, aReturnStream );
		_destination = _destinationSegment.getPayload();
	}
}
