// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.p2p.alt.serial;

import static org.refcodes.serial.SerialSugar.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.refcodes.exception.UnmarshalException;
import org.refcodes.p2p.AbstractP2PMessage;
import org.refcodes.p2p.P2PMessage;
import org.refcodes.serial.AllocSectionDecoratorSegment;
import org.refcodes.serial.ByteArraySequence;
import org.refcodes.serial.ComplexTypeSegment;
import org.refcodes.serial.Segment;
import org.refcodes.serial.Segment.SegmentMixin;
import org.refcodes.serial.Sequence;
import org.refcodes.serial.SequenceSection;
import org.refcodes.serial.SerialSchema;
import org.refcodes.serial.TransmissionException;
import org.refcodes.struct.SimpleTypeMap;

/**
 * Basic implementation of the {@link P2PMessage} optimized for local area
 * microcontroller wiring (networks) and microcontroller memory layout: The hops
 * are serialized and deserialized in the tail of the {@link SerialP2PMessage}
 * as this makes append the last hop easier for devices with small memory
 * buffers.
 */
public class SerialP2PMessage extends AbstractP2PMessage<Integer, SerialP2PHeader, Sequence, SerialP2PTail> implements Segment, SegmentMixin {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final SerialP2PTransmissionMetrics DEFAULT_TRANSMISSION_METRICS = new SerialP2PTransmissionMetrics();

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private Segment _delegatee;
	private transient Segment _payloadSegment;
	private SerialP2PTransmissionMetrics _transmissionMetrics;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an empty {@link SerialP2PMessage}.
	 */
	public SerialP2PMessage() {
		this( -1, -1, new ByteArraySequence(), DEFAULT_TRANSMISSION_METRICS );
	}

	/**
	 * Constructs an empty {@link SerialP2PMessage}.
	 *
	 * @param aTransmissionMetrics the a {@link SerialP2PTransmissionMetrics} to
	 *        use.
	 */
	public SerialP2PMessage( SerialP2PTransmissionMetrics aTransmissionMetrics ) {
		this( -1, -1, new ByteArraySequence(), aTransmissionMetrics );
	}

	/**
	 * Constructs an {@link SerialP2PMessage}. The source is added upon
	 * transmission as first stop-over.
	 * 
	 * @param <P> The type of the payload in question.
	 * @param aDestination The destination locator of the peer being addressed.
	 * @param aPayload The payload to be sent.
	 */
	public <P> SerialP2PMessage( int aDestination, P aPayload ) {
		this( -1, aDestination, aPayload, DEFAULT_TRANSMISSION_METRICS );
	}

	/**
	 * Constructs an {@link SerialP2PMessage}. The source is added upon
	 * transmission as first stop-over.
	 *
	 * @param <P> The type of the payload in question.
	 * @param aDestination The destination locator of the peer being addressed.
	 * @param aPayload The payload to be sent.
	 * @param aTransmissionMetrics the a {@link SerialP2PTransmissionMetrics} to
	 *        use.
	 */
	public <P> SerialP2PMessage( int aDestination, P aPayload, SerialP2PTransmissionMetrics aTransmissionMetrics ) {
		this( -1, aDestination, complexTypeSegment( aPayload, aTransmissionMetrics ), aTransmissionMetrics );
	}

	/**
	 * Constructs an {@link SerialP2PMessage}. The source is added upon
	 * transmission as first stop-over.
	 *
	 * @param <P> The type of the payload in question.
	 * @param aMagicNumber The magic number for tagging this message when being
	 *        transmitted.
	 * @param aDestination The destination locator of the peer being addressed.
	 * @param aPayload The payload to be sent.
	 */
	public <P> SerialP2PMessage( int aMagicNumber, int aDestination, P aPayload ) {
		this( aMagicNumber, aDestination, complexTypeSegment( aPayload, DEFAULT_TRANSMISSION_METRICS ), DEFAULT_TRANSMISSION_METRICS );
	}

	/**
	 * Constructs an {@link SerialP2PMessage}. The source is added upon
	 * transmission as first stop-over.
	 *
	 * @param <P> The type of the payload in question.
	 * @param aMagicNumber The magic number for tagging this message when being
	 *        transmitted.
	 * @param aDestination The destination locator of the peer being addressed.
	 * @param aPayload The payload to be sent.
	 * @param aTransmissionMetrics the a {@link SerialP2PTransmissionMetrics} to
	 *        use.
	 */
	public <P> SerialP2PMessage( int aMagicNumber, int aDestination, P aPayload, SerialP2PTransmissionMetrics aTransmissionMetrics ) {
		this( aMagicNumber, aDestination, complexTypeSegment( aPayload, aTransmissionMetrics ), aTransmissionMetrics );
	}

	/**
	 * Constructs an {@link SerialP2PMessage}. The source is added upon
	 * transmission as first stop-over.
	 *
	 * @param aDestination The destination locator of the peer being addressed.
	 * @param aPayload the a payload
	 */
	public SerialP2PMessage( int aDestination, Sequence aPayload ) {
		this( -1, aDestination, aPayload, DEFAULT_TRANSMISSION_METRICS );
	}

	/**
	 * Constructs an {@link SerialP2PMessage}. The source is added upon
	 * transmission as first stop-over.
	 *
	 * @param aMagicNumber The magic number for tagging this message when being
	 *        transmitted.
	 * @param aDestination The destination locator of the peer being addressed.
	 * @param aPayload the a payload
	 */
	public SerialP2PMessage( int aMagicNumber, int aDestination, Segment aPayload ) {
		this( aMagicNumber, aDestination, aPayload, DEFAULT_TRANSMISSION_METRICS );
	}

	/**
	 * Constructs an {@link SerialP2PMessage}. The source is added upon
	 * transmission as first stop-over.
	 *
	 * @param aDestination The destination locator of the peer being addressed.
	 * @param aPayload the a payload
	 */
	public SerialP2PMessage( int aDestination, Segment aPayload ) {
		this( -1, aDestination, aPayload, DEFAULT_TRANSMISSION_METRICS );
	}

	/**
	 * Constructs an {@link SerialP2PMessage}. The source is added upon
	 * transmission as first stop-over.
	 *
	 * @param aMagicNumber The magic number for tagging this message when being
	 *        transmitted.
	 * @param aDestination The destination locator of the peer being addressed.
	 * @param aPayload The payload to be sent.
	 */
	public SerialP2PMessage( int aMagicNumber, int aDestination, Sequence aPayload ) {
		this( aMagicNumber, aDestination, aPayload, DEFAULT_TRANSMISSION_METRICS );
	}

	/**
	 * Constructs an {@link SerialP2PMessage}. The source is added upon
	 * transmission as first stop-over.
	 * 
	 * @param aDestination The destination locator of the peer being addressed.
	 * @param aPayload the a payload
	 * @param aTransmissionMetrics the a {@link SerialP2PTransmissionMetrics} to
	 *        use.
	 */
	public SerialP2PMessage( int aDestination, Segment aPayload, SerialP2PTransmissionMetrics aTransmissionMetrics ) {
		this( -1, aDestination, aPayload, aTransmissionMetrics );
	}

	/**
	 * Constructs an {@link SerialP2PMessage}. The source is added upon
	 * transmission as first stop-over.
	 * 
	 * @param aMagicNumber The magic number for tagging this message when being
	 *        transmitted..
	 * @param aDestination The destination locator of the peer being addressed.
	 * @param aPayload the a payload
	 * @param aTransmissionMetrics the a {@link SerialP2PTransmissionMetrics} to
	 *        use.
	 */
	public SerialP2PMessage( int aMagicNumber, int aDestination, Segment aPayload, SerialP2PTransmissionMetrics aTransmissionMetrics ) {
		super( new SerialP2PHeader( aMagicNumber, aDestination, aTransmissionMetrics ), aPayload.toSequence(), new SerialP2PTail() );
		_payloadSegment = aPayload;
		_transmissionMetrics = aTransmissionMetrics != null ? aTransmissionMetrics : DEFAULT_TRANSMISSION_METRICS;

		// @formatter:off
		final Segment theBodySegment = crcSuffixSegment(
			_payloadSegment, _transmissionMetrics.getCrcAlgorithm()
		);
		// @formatter:on

		_delegatee = segmentComposite( _header, theBodySegment, _tail );
	}

	/**
	 * Constructs an {@link SerialP2PMessage}. The source is added upon
	 * transmission as first stop-over.
	 * 
	 * @param aDestination The destination locator of the peer being addressed.
	 * @param aPayload The payload to be sent.
	 * @param aTransmissionMetrics the a {@link SerialP2PTransmissionMetrics} to
	 *        use.
	 */
	public SerialP2PMessage( int aDestination, Sequence aPayload, SerialP2PTransmissionMetrics aTransmissionMetrics ) {
		this( -1, aDestination, aPayload, aTransmissionMetrics );
	}

	/**
	 * Constructs an {@link SerialP2PMessage}. The source is added upon
	 * transmission as first stop-over.
	 * 
	 * @param aMagicNumber The magic number for tagging this message when being
	 *        transmitted.
	 * @param aDestination The destination locator of the peer being addressed.
	 * @param aPayload The payload to be sent.
	 * @param aTransmissionMetrics the a {@link SerialP2PTransmissionMetrics} to
	 *        use.
	 */
	public SerialP2PMessage( int aMagicNumber, int aDestination, Sequence aPayload, SerialP2PTransmissionMetrics aTransmissionMetrics ) {
		super( new SerialP2PHeader( aMagicNumber, aDestination, aTransmissionMetrics ), aPayload, new SerialP2PTail() );
		final SequenceSection theSequenceSection = aPayload != null ? sequenceSection( aPayload ) : sequenceSection();
		final AllocSectionDecoratorSegment<SequenceSection> thePayload = allocSegment( theSequenceSection );
		_payloadSegment = thePayload;
		_transmissionMetrics = aTransmissionMetrics != null ? aTransmissionMetrics : DEFAULT_TRANSMISSION_METRICS;

		// @formatter:off
		final Segment theBodySegment = crcSuffixSegment(
			_payloadSegment , _transmissionMetrics.getCrcAlgorithm()
		);
		// @formatter:on

		_delegatee = segmentComposite( _header, theBodySegment, _tail );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int fromTransmission( Sequence aSequence, int aOffset ) throws TransmissionException {
		final int theOffset = _delegatee.fromTransmission( aSequence, aOffset );
		_body = _payloadSegment.toSequence();
		return theOffset;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void receiveFrom( InputStream aInputStream, OutputStream aFeedbackStream ) throws IOException {
		_delegatee.receiveFrom( aInputStream, aFeedbackStream );
		_body = _payloadSegment.toSequence();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence toSequence() {
		return _delegatee.toSequence();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void transmitTo( OutputStream aOutputStream, InputStream aReturnStream ) throws IOException {
		_delegatee.transmitTo( aOutputStream, aReturnStream );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() { /* this is an immutable type */ }

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SerialSchema toSchema() {
		return _delegatee.toSchema();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getLength() {
		return _delegatee.getLength();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SimpleTypeMap toSimpleTypeMap() {
		return _delegatee.toSimpleTypeMap();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <P> P getPayload( Class<P> aResponseType ) throws UnmarshalException {
		final ComplexTypeSegment<P> thePayloadSegment = complexTypeSegment( aResponseType, _transmissionMetrics );
		Sequence theSequence = null;
		try {
			theSequence = _payloadSegment.toSequence();
			thePayloadSegment.fromTransmission( theSequence );
		}
		catch ( TransmissionException e ) {
			throw new UnmarshalException( "Cannot unmarshal the payload " + ( theSequence != null ? "<" + theSequence.toHexString() + "> " : "" ) + "to type <" + aResponseType.getName() + ">!", e );
		}
		return thePayloadSegment.getPayload();
	}
}
