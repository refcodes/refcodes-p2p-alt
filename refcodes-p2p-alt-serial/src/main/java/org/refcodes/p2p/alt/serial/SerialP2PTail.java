// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.p2p.alt.serial;

import static org.refcodes.serial.SerialSugar.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.refcodes.p2p.AbstractP2PTail;
import org.refcodes.p2p.P2PHeader;
import org.refcodes.serial.IntArraySection;
import org.refcodes.serial.Section;
import org.refcodes.serial.Segment;
import org.refcodes.serial.Segment.SegmentMixin;
import org.refcodes.serial.Sequence;
import org.refcodes.serial.SerialSchema;
import org.refcodes.serial.TransmissionException;
import org.refcodes.serial.TransmissionMetrics;
import org.refcodes.struct.SimpleTypeMap;

/**
 * The {@link SerialP2PTail} represents a {@link P2PHeader} optimized for local
 * microcontroller wiring (networks) and microcontroller memory layout: The hops
 * (as of {@link #getHops()} are serialized and deserialized in the tail of the
 * {@link SerialP2PMessage} suffixed with the CRC checksum, as this makes append
 * the last hop and recalculate the CRC checksum easier for devices with small
 * memory buffers.
 */
public class SerialP2PTail extends AbstractP2PTail<Integer> implements Segment, SegmentMixin {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final TransmissionMetrics DEFAULT_TRANSMISSION_METRICS = new TransmissionMetrics();

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private Segment _delegatee;
	private IntArraySection _hopsSection;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the tail using. The source is added upon transmission as first
	 * stop-over.
	 */
	SerialP2PTail() {
		this( DEFAULT_TRANSMISSION_METRICS );
	}

	/**
	 * Constructs the tail using. The source is added upon transmission as first
	 * stop-over.
	 * 
	 * @param aTransmissionMetrics the a {@link TransmissionMetrics} to use.
	 */
	SerialP2PTail( TransmissionMetrics aTransmissionMetrics ) {
		aTransmissionMetrics = aTransmissionMetrics != null ? aTransmissionMetrics : DEFAULT_TRANSMISSION_METRICS;
		_hops = new Integer[0];
		// @formatter:off
		_delegatee = crcSuffixSegment(  
			allocSegment(
				_hopsSection = intArraySection( aTransmissionMetrics ), aTransmissionMetrics
			), 	aTransmissionMetrics
		);
		// @formatter:on

	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getLength() {
		updateHopsSection();
		return _delegatee.getLength();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence toSequence() {
		updateHopsSection();
		return _delegatee.toSequence();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void transmitTo( OutputStream aOutputStream, InputStream aReturnStream ) throws IOException {
		updateHopsSection();
		_delegatee.transmitTo( aOutputStream, aReturnStream );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() {
		_hops = null;
		_hopsSection.reset();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SerialSchema toSchema() {
		updateHopsSection();
		return _delegatee.toSchema();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SimpleTypeMap toSimpleTypeMap() {
		updateHopsSection();
		return _delegatee.toSimpleTypeMap();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int fromTransmission( Sequence aSequence, int aOffset ) throws TransmissionException {
		final int theOffset = _delegatee.fromTransmission( aSequence, aOffset );
		updateHops();
		return theOffset;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void receiveFrom( InputStream aInputStream, OutputStream aReturnStream ) throws IOException {
		_delegatee.receiveFrom( aInputStream, aReturnStream );
		updateHops();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void appendHop( Integer aLocator ) {
		super.appendHop( aLocator );
		updateHopsSection();
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Updates the hops from the hops {@link Section}.
	 */
	void updateHops() {
		setHops( _hopsSection.getPayload() );
	}

	/**
	 * Updates the hops {@link Section} from the hops.
	 */
	void updateHopsSection() {
		_hopsSection.setPayload( getHops() );
	}

	/**
	 * Retrieves the hops {@link Section}.
	 */
	Section getHopsSection() {
		return _hopsSection;
	}

	/**
	 * Hook to set the stop-over trail e.g. upon receiving serial data from a
	 * port.
	 * 
	 * @param aHops The stop over trail to be set.
	 */
	void setHops( int[] aHops ) {
		_hops = toBoxedArray( aHops );
	}

	/**
	 * Hook to set the stop-over trail e.g. upon receiving serial data from a
	 * port.
	 * 
	 * @param aHops The stop over trail to be set.
	 */
	void setHops( Integer[] aHops ) {
		_hops = aHops;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private static Integer[] toBoxedArray( int[] aValues ) {
		final Integer[] theValues = new Integer[aValues.length];
		for ( int i = 0; i < theValues.length; i++ ) {
			theValues[i] = aValues[i];
		}
		return theValues;
	}
}
