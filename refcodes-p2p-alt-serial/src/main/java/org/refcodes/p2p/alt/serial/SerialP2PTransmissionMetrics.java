// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.p2p.alt.serial;

import java.nio.charset.Charset;

import org.refcodes.data.Ascii;
import org.refcodes.data.IoReconnectLoopTime;
import org.refcodes.mixin.ConcatenateMode;
import org.refcodes.numerical.ChecksumValidationMode;
import org.refcodes.numerical.CrcAlgorithm;
import org.refcodes.numerical.Endianess;
import org.refcodes.p2p.alt.serial.SerialPeerProxy.HopCountRequest;
import org.refcodes.p2p.alt.serial.SerialPeerProxy.HopCountResponse;
import org.refcodes.p2p.alt.serial.SerialPeerProxy.SerialP2PMessageResponse;
import org.refcodes.serial.SegmentPackager;
import org.refcodes.serial.TransmissionMetrics;
import org.refcodes.serial.ext.handshake.HandshakePortController;
import org.refcodes.serial.ext.handshake.HandshakeTransmissionMetrics;

/**
 * The {@link SerialP2PTransmissionMetrics} provides additional metrics for the
 * {@link SerialPeerProxy} to communicate over the
 * {@link HandshakePortController}.
 */
public class SerialP2PTransmissionMetrics extends HandshakeTransmissionMetrics {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final byte[] DEFAULT_HOP_COUNT_REQUEST_MAGIC_BYTES = new byte[] { Ascii.ENQ.getCode() };
	private static final byte[] DEFAULT_HOP_COUNT_RESPONSE_MAGIC_BYTES = new byte[] { Ascii.ACK.getCode() };
	private static final byte[] DEFAULT_P2P_MESSAGE_MAGIC_BYTES = new byte[] { Ascii.SOH.getCode() };
	private static final byte[] DEFAULT_P2P_MESSAGE_RESPONSE_MAGIC_BYTES = new byte[] { Ascii.EOT.getCode() };

	private static final AcknowledgeMode DEFAULT_ACKNOWLEDGE_MODE = AcknowledgeMode.ON;
	private static final int DEFAULT_IO_RECONNETCT_LOOP_TIME_IN_MS = IoReconnectLoopTime.MIN.getTimeMillis();

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private byte[] _hopCountRequestMagicBytes = DEFAULT_HOP_COUNT_REQUEST_MAGIC_BYTES;
	private byte[] _hopCountResponseMagicBytes = DEFAULT_HOP_COUNT_RESPONSE_MAGIC_BYTES;
	private byte[] _p2pMessageMagicBytes = DEFAULT_P2P_MESSAGE_MAGIC_BYTES;
	private byte[] _p2pMessageResponseMagicBytes = DEFAULT_P2P_MESSAGE_RESPONSE_MAGIC_BYTES;

	private AcknowledgeMode _acknowledgeMode = DEFAULT_ACKNOWLEDGE_MODE;
	private int _ioReconnetctLoopTimeInMs = DEFAULT_IO_RECONNETCT_LOOP_TIME_IN_MS;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	protected SerialP2PTransmissionMetrics( Builder aBuilder ) {
		super( aBuilder );
		_hopCountRequestMagicBytes = aBuilder.hopCountRequestMagicBytes;
		_hopCountResponseMagicBytes = aBuilder.hopCountResponseMagicBytes;
		_p2pMessageMagicBytes = aBuilder.p2pMessageMagicBytes;
		_p2pMessageResponseMagicBytes = aBuilder.p2pMessageResponseMagicBytes;
		_acknowledgeMode = aBuilder.acknowledgeMode;
		_ioReconnetctLoopTimeInMs = aBuilder.ioReconnetctLoopTimeInMs;
	}

	// -------------------------------------------------------------------------

	/**
	 * Creates an instance of the {@link SerialP2PTransmissionMetrics} with
	 * default values being applied as defined in the
	 * {@link SerialP2PTransmissionMetrics} type.
	 */
	public SerialP2PTransmissionMetrics() {}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Returns the I/O reconnect time in milliseconds to wait till trying to
	 * reconnect (upon a closed connection or or a remote not up and running
	 * properly).
	 * 
	 * @return The according reconnect delay time in milliseconds.
	 */
	public int getIoReconnetctLoopTimeInMs() {
		return _ioReconnetctLoopTimeInMs;
	}

	/**
	 * Returns the {@link AcknowledgeMode} to be applied.
	 * 
	 * @return The according {@link AcknowledgeMode}.
	 */
	public AcknowledgeMode getAcknowledgeMode() {
		return _acknowledgeMode;
	}

	/**
	 * Returns the magic bytes as required by the {@link HopCountRequest}
	 * transmission.
	 * 
	 * @return The according magic bytes.
	 */
	public byte[] getHopCountRequestMagicBytes() {
		return _hopCountRequestMagicBytes;
	}

	/**
	 * Returns the magic bytes as required by the {@link HopCountResponse}
	 * transmission.
	 * 
	 * @return The according magic bytes.
	 */
	public byte[] getHopCountResponseMagicBytes() {
		return _hopCountResponseMagicBytes;
	}

	/**
	 * Returns the magic bytes as required by the {@link SerialP2PMessage}
	 * transmission.
	 * 
	 * @return The according magic bytes.
	 */
	public byte[] getP2PMessageMagicBytes() {
		return _p2pMessageMagicBytes;
	}

	/**
	 * Returns the magic bytes as required by the
	 * {@link SerialP2PMessageResponse} transmission.
	 * 
	 * @return The according magic bytes.
	 */
	public byte[] getP2PMessageResponseMagicBytes() {
		return _p2pMessageResponseMagicBytes;
	}

	/**
	 * Creates builder to build {@link SerialP2PTransmissionMetrics}.
	 * 
	 * @return created builder
	 */
	public static Builder builder() {
		return new Builder();
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Builder to build {@link TransmissionMetrics} instances.
	 */
	public static final class Builder extends HandshakeTransmissionMetrics.Builder {

		protected byte[] hopCountRequestMagicBytes = DEFAULT_HOP_COUNT_REQUEST_MAGIC_BYTES;
		protected byte[] hopCountResponseMagicBytes = DEFAULT_HOP_COUNT_RESPONSE_MAGIC_BYTES;
		protected byte[] p2pMessageMagicBytes = DEFAULT_P2P_MESSAGE_MAGIC_BYTES;
		protected byte[] p2pMessageResponseMagicBytes = DEFAULT_P2P_MESSAGE_RESPONSE_MAGIC_BYTES;
		protected AcknowledgeMode acknowledgeMode = DEFAULT_ACKNOWLEDGE_MODE;
		protected int ioReconnetctLoopTimeInMs = DEFAULT_IO_RECONNETCT_LOOP_TIME_IN_MS;

		protected Builder() {}

		/**
		 * Sets the I/O reconnect time in milliseconds to wait till trying to
		 * reconnect (upon a closed connection or or a remote not up and running
		 * properly).
		 * 
		 * @param aIoReconnetctLoopTimeInMs The according reconnect delay time
		 *        in milliseconds.
		 * 
		 * @return This {@link Builder} as of the builder pattern.
		 */
		public Builder withIoReconnetctLoopTimeInMs( int aIoReconnetctLoopTimeInMs ) {
			ioReconnetctLoopTimeInMs = aIoReconnetctLoopTimeInMs;
			return this;
		}

		/**
		 * Sets the {@link AcknowledgeMode} to be applied..
		 * 
		 * @param aAcknowledgeMode The according {@link AcknowledgeMode}.
		 * 
		 * @return This {@link Builder} as of the builder pattern.
		 */
		public Builder withAcknowledgeMode( AcknowledgeMode aAcknowledgeMode ) {
			acknowledgeMode = aAcknowledgeMode;
			return this;
		}

		/**
		 * Sets the number of retries from the reply retry number. A reply retry
		 * number is the overall number of retries to use when counting reply
		 * retries.
		 * 
		 * @param aReplyRetryNumber The number of retries stored by the reply
		 *        retry number.
		 * 
		 * @return Returns this {@link Builder} as of the builder pattern.
		 */
		@Override
		public Builder withReplyRetryNumber( int aReplyRetryNumber ) {
			replyRetryNumber = aReplyRetryNumber;
			return this;
		}

		/**
		 * The reply timeout attribute in milliseconds.
		 * 
		 * @param aReplyTimeoutInMs An long integer with the timeout in
		 *        milliseconds.
		 * 
		 * @return Returns this {@link Builder} as of the builder pattern.
		 */
		@Override
		public Builder withReplyTimeoutMillis( long aReplyTimeoutInMs ) {
			replyTimeoutInMs = aReplyTimeoutInMs;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withIoHeuristicsTimeToLiveMillis( long aIoHeuristicsTimeToLiveInMs ) {
			ioHeuristicsTimeToLiveInMs = aIoHeuristicsTimeToLiveInMs;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withPongMagicBytes( byte[] aPongMagicBytes ) {
			pongMagicBytes = aPongMagicBytes;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withPingMagicBytes( byte[] aPingMagicBytes ) {
			pingMagicBytes = aPingMagicBytes;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withPingRetryNumber( int aPingRetryNumber ) {
			pingRetryNumber = aPingRetryNumber;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withPingTimeoutMillis( long aPingTimeoutInMs ) {
			pingTimeoutInMs = aPingTimeoutInMs;
			return this;
		}

		/**
		 * Returns the magic bytes as required by the {@link HopCountRequest}
		 * transmission.
		 *
		 * @param aHopCountRequestMagicBytes the hop count request magic bytes
		 * 
		 * @return This {@link Builder} as of the builder pattern.
		 */
		public Builder withHopCountRequestMagicBytes( byte[] aHopCountRequestMagicBytes ) {
			return this;
		}

		/**
		 * Returns the magic bytes as required by the {@link HopCountResponse}
		 * transmission.
		 *
		 * @param aHopCountResponseMagicBytes the hop count response magic bytes
		 * 
		 * @return This {@link Builder} as of the builder pattern.
		 */
		public Builder withHopCountResponseMagicBytes( byte[] aHopCountResponseMagicBytes ) {
			return this;
		}

		/**
		 * Returns the magic bytes as required by the {@link SerialP2PMessage}
		 * transmission.
		 *
		 * @param aP2PMessageMagicBytes the p 2 P message magic bytes
		 * 
		 * @return This {@link Builder} as of the builder pattern.
		 */
		public Builder withP2PMessageMagicBytes( byte[] aP2PMessageMagicBytes ) {
			return this;
		}

		/**
		 * Returns the magic bytes as required by the
		 * {@link SerialP2PMessageResponse} transmission.
		 *
		 * @param aP2PMessageResponseMagicBytes the p 2 P message response magic
		 *        bytes
		 * 
		 * @return This {@link Builder} as of the builder pattern.
		 */
		public Builder withP2PMessageResponseMagicBytes( byte[] aP2PMessageResponseMagicBytes ) {
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withAcknowledgeMagicBytes( byte[] aAcknowledgeMagicBytes ) {
			acknowledgeMagicBytes = aAcknowledgeMagicBytes;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withClearToSendMagicBytes( byte[] aClearToSendMagicBytes ) {
			clearToSendMagicBytes = aClearToSendMagicBytes;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withPacketMagicBytes( byte[] aPacketMagicBytes ) {
			packetMagicBytes = aPacketMagicBytes;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withReadyToReceiveMagicBytes( byte[] aReadyToReceiveMagicBytes ) {
			readyToReceiveMagicBytes = aReadyToReceiveMagicBytes;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withReadyToSendMagicBytes( byte[] aReadyToSendMagicBytes ) {
			readyToSendMagicBytes = aReadyToSendMagicBytes;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withAcknowledgeRetryNumber( int aAcknowledgeRetryNumber ) {
			acknowledgeRetryNumber = aAcknowledgeRetryNumber;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withAcknowledgeSegmentPackager( SegmentPackager aAcknowledgeSegmentPackager ) {
			acknowledgeSegmentPackager = aAcknowledgeSegmentPackager;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withAcknowledgeTimeoutMillis( long aAcknowledgeTimeoutInMs ) {
			acknowledgeTimeoutInMs = aAcknowledgeTimeoutInMs;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withBlockSize( int aBlockSize ) {
			blockSize = aBlockSize;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withChecksumValidationMode( ChecksumValidationMode aChecksumValidationMode ) {
			checksumValidationMode = aChecksumValidationMode;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withClearToSendSegmentPackager( SegmentPackager aClearToSendSegmentPackager ) {
			clearToSendSegmentPackager = aClearToSendSegmentPackager;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withClearToSendTimeoutMillis( long aClearToSendTimeoutInMs ) {
			clearToSendTimeoutInMs = aClearToSendTimeoutInMs;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withCrcAlgorithm( CrcAlgorithm aCrcAlgorithm ) {
			crcAlgorithm = aCrcAlgorithm;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withCrcChecksumConcatenateMode( ConcatenateMode aChecksumConcatenateMode ) {
			crcChecksumConcatenateMode = aChecksumConcatenateMode;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withEncoding( Charset aEncoding ) {
			encoding = aEncoding;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withEndianess( Endianess aEndianess ) {
			endianess = aEndianess;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withEndOfStringByte( byte aEndOfStringByte ) {
			endOfStringByte = aEndOfStringByte;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withEnquiryStandbyTimeMillis( long aEnquiryStandbyTimeInMs ) {
			enquiryStandbyTimeInMs = aEnquiryStandbyTimeInMs;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withLengthWidth( int aLengthWidth ) {
			lengthWidth = aLengthWidth;
			return this;
		}

		/**
		 * With packet length width.
		 *
		 * @param aPacketLengthWidth the packet length width
		 * 
		 * @return the builder
		 */
		@Override
		public Builder withPacketLengthWidth( int aPacketLengthWidth ) {
			packetLengthWidth = aPacketLengthWidth;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withMagicBytesLength( int aMagicBytesLength ) {
			magicBytesLength = aMagicBytesLength;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withPacketSegmentPackager( SegmentPackager aPacketSegmentPackager ) {
			packetSegmentPackager = aPacketSegmentPackager;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withReadTimeoutMillis( long aReadTimeoutInMs ) {
			readTimeoutInMs = aReadTimeoutInMs;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withReadyToReceiveRetryNumber( int aReadyToReceiveRetryNumber ) {
			readyToReceiveRetryNumber = aReadyToReceiveRetryNumber;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withReadyToReceiveSegmentPackager( SegmentPackager aReadyToReceiveSegmentPackager ) {
			readyToReceiveSegmentPackager = aReadyToReceiveSegmentPackager;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withReadyToReceiveTimeoutMillis( long aReadyToReceiveTimeoutInMs ) {
			readyToReceiveTimeoutInMs = aReadyToReceiveTimeoutInMs;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withReadyToSendRetryNumber( int aReadyToSendRetryNumber ) {
			readyToSendRetryNumber = aReadyToSendRetryNumber;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withReadyToSendSegmentPackager( SegmentPackager aReadyToSendSegmentPackager ) {
			readyToSendSegmentPackager = aReadyToSendSegmentPackager;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withReadyToSendTimeoutMillis( long aReadyToSendTimeoutInMs ) {
			readyToSendTimeoutInMs = aReadyToSendTimeoutInMs;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withSequenceNumberConcatenateMode( ConcatenateMode aSequenceNumberConcatenateMode ) {
			sequenceNumberConcatenateMode = aSequenceNumberConcatenateMode;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withSequenceNumberInitValue( int aSequenceNumberInitValue ) {
			sequenceNumberInitValue = aSequenceNumberInitValue;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withSequenceNumberWidth( int aSequenceNumberWidth ) {
			sequenceNumberWidth = aSequenceNumberWidth;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withTransmissionRetryNumber( int aTransmissionRetryNumber ) {
			transmissionRetryNumber = aTransmissionRetryNumber;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withTransmissionTimeoutMillis( long aTransmissionTimeoutInMs ) {
			transmissionTimeoutInMs = aTransmissionTimeoutInMs;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withWriteTimeoutMillis( long aWriteTimeoutInMs ) {
			writeTimeoutInMs = aWriteTimeoutInMs;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withAcknowledgeableTransmissionMagicBytes( byte[] aAcknowledgeableTransmissionMagicBytes ) {
			acknowledgeableTransmissionMagicBytes = aAcknowledgeableTransmissionMagicBytes;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withAcknowledgeableResponseMagicBytes( byte[] aAcknowledgeableResponseMagicBytes ) {
			acknowledgeableResponseMagicBytes = aAcknowledgeableResponseMagicBytes;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withAcknowledgeableRequestMagicBytes( byte[] aAcknowledgeableRequestMagicBytes ) {
			acknowledgeableRequestMagicBytes = aAcknowledgeableRequestMagicBytes;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withTransmissionMagicBytes( byte[] aTransmissionMagicBytes ) {
			transmissionMagicBytes = aTransmissionMagicBytes;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withResponseMagicBytes( byte[] aResponseMagicBytes ) {
			responseMagicBytes = aResponseMagicBytes;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withRequestMagicBytes( byte[] aRequestMagicBytes ) {
			requestMagicBytes = aRequestMagicBytes;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withTransmissionDismissedMagicBytes( byte[] aTransmissionDismissedMagicBytes ) {
			transmissionDismissedMagicBytes = aTransmissionDismissedMagicBytes;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withRequestDismissedMagicBytes( byte[] aRequestDismissedMagicBytes ) {
			requestDismissedMagicBytes = aRequestDismissedMagicBytes;
			return this;
		}

		/**
		 * Builds the {@link HandshakeTransmissionMetrics} instance from this
		 * builder's settings.
		 *
		 * @return The accordingly built {@link HandshakeTransmissionMetrics}
		 *         instance.
		 */
		@Override
		public SerialP2PTransmissionMetrics build() {
			return new SerialP2PTransmissionMetrics( this );
		}
	}
}
