// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.p2p.alt.serial;

import java.io.IOException;
import java.util.concurrent.ExecutorService;

import org.refcodes.controlflow.ControlFlowUtility;
import org.refcodes.p2p.AbstractPeer;
import org.refcodes.p2p.NoSuchDestinationException;
import org.refcodes.p2p.P2PMessage;
import org.refcodes.p2p.P2PMessageConsumer;
import org.refcodes.p2p.Peer;
import org.refcodes.serial.Port;
import org.refcodes.serial.Segment;
import org.refcodes.serial.Sequence;
import org.refcodes.serial.ext.handshake.HandshakePortController;

/**
 * The {@link SerialPeer} implements a {@link Peer} optimized for local area
 * microcontroller wiring (networks) and microcontroller memory layout.
 */
public class SerialPeer extends AbstractPeer<Integer, SerialP2PHeader, SerialP2PTail, SerialP2PMessage, P2PMessageConsumer<SerialP2PMessage, SerialPeer>, SerialPeer, SerialPeerRouter> implements SerialPeerRouter {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private SerialP2PTransmissionMetrics _transmissionMetrics;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the {@link SerialPeer} instance with the given initial state.
	 *
	 * @param aLocator The LOCATOR of the {@link Peer} being constructed.
	 *        Messages targeted to this {@link Peer} will be addressed to the
	 *        given LOCATOR.
	 * @param aMessageConsumer The consumer (being the functional
	 *        {@link P2PMessageConsumer} interface) of the {@link P2PMessage}
	 *        instances targeted at this {@link Peer} instance.
	 * @param aPorts the ports
	 */
	//	SerialPeer( Integer aLocator, SerialP2PMessageConsumer aMessageConsumer ) {
	//		super( aLocator, aMessageConsumer );
	//	}

	/**
	 * Constructs the {@link Peer} instance with the given locator identifying
	 * the {@link Peer} and the given {@link P2PMessageConsumer} processing the
	 * messages targeted at the given locator binding the given {@link Port}
	 * instances.
	 * 
	 * @param aLocator The LOCATOR of the {@link Peer} being constructed.
	 *        Messages targeted to this {@link Peer} will be addressed to the
	 *        given LOCATOR.
	 * @param aMessageConsumer The consumer (being the functional
	 *        {@link P2PMessageConsumer} interface) of the {@link P2PMessage}
	 *        instances targeted at this {@link Peer} instance.
	 * @param aPorts The {@link Port} instances to which to attach.
	 */
	public SerialPeer( Integer aLocator, SerialP2PMessageConsumer aMessageConsumer, Port<?>... aPorts ) {
		this( aLocator, aMessageConsumer, null, null, aPorts );
	}

	/**
	 * Constructs the {@link Peer} instance with the given locator identifying
	 * the {@link Peer} and the given {@link P2PMessageConsumer} processing the
	 * messages targeted at the given locator binding the given {@link Port}
	 * instances.
	 * 
	 * @param aLocator The LOCATOR of the {@link Peer} being constructed.
	 *        Messages targeted to this {@link Peer} will be addressed to the
	 *        given LOCATOR.
	 * @param aMessageConsumer The consumer (being the functional
	 *        {@link P2PMessageConsumer} interface) of the {@link P2PMessage}
	 *        instances targeted at this {@link Peer} instance.
	 * @param aTransmissionMetrics The {@link SerialP2PTransmissionMetrics} to
	 *        use when setting up the peer.
	 * @param aPorts The {@link Port} instances to which to attach.
	 */
	public SerialPeer( Integer aLocator, SerialP2PMessageConsumer aMessageConsumer, SerialP2PTransmissionMetrics aTransmissionMetrics, Port<?>... aPorts ) {
		this( aLocator, aMessageConsumer, aTransmissionMetrics, null, aPorts );
	}

	/**
	 * Constructs the {@link Peer} instance with the given locator identifying
	 * the {@link Peer} and the given {@link P2PMessageConsumer} processing the
	 * messages targeted at the given locator binding the given {@link Port}
	 * instances.
	 * 
	 * @param aLocator The LOCATOR of the {@link Peer} being constructed.
	 *        Messages targeted to this {@link Peer} will be addressed to the
	 *        given LOCATOR.
	 * @param aMessageConsumer The consumer (being the functional
	 *        {@link P2PMessageConsumer} interface) of the {@link P2PMessage}
	 *        instances targeted at this {@link Peer} instance.
	 * @param aTransmissionMetrics The {@link SerialP2PTransmissionMetrics} to
	 *        use when setting up the peer.
	 * @param aExecutorService The {@link ExecutorService} to be used when
	 *        creating {@link Thread} instances for handling input and output
	 *        data simultaneously.
	 * @param aPorts The {@link Port} instances to which to attach.
	 */
	public SerialPeer( Integer aLocator, SerialP2PMessageConsumer aMessageConsumer, SerialP2PTransmissionMetrics aTransmissionMetrics, ExecutorService aExecutorService, Port<?>... aPorts ) {
		super( aLocator, aMessageConsumer );
		_transmissionMetrics = aTransmissionMetrics != null ? aTransmissionMetrics : new SerialP2PTransmissionMetrics();
		final ExecutorService theExecutorService = ( aExecutorService != null ) ? aExecutorService : ControlFlowUtility.createCachedExecutorService( true );
		for ( Port<?> ePort : aPorts ) {
			addPeerRouter( new SerialPeerProxy( new HandshakePortController<>( ePort, _transmissionMetrics, theExecutorService ), this, _transmissionMetrics ) );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <P> void sendMessage( Integer aDestination, P aPayload ) throws IOException, NoSuchDestinationException {
		sendMessage( new SerialP2PMessage( aDestination, aPayload, _transmissionMetrics ) );
	}

	/**
	 * Convenience method for {@link #sendMessage(P2PMessage)}.
	 *
	 * @param aDestination The destination of the message.
	 * @param aPayload The payload to be sent.
	 * 
	 * @throws IOException thrown in case I/O problems occurred while accessing
	 *         the mesh.
	 * @throws NoSuchDestinationException thrown in case there is none such
	 *         destination peer.
	 */
	public void sendMessage( int aDestination, Segment aPayload ) throws IOException, NoSuchDestinationException {
		sendMessage( new SerialP2PMessage( aDestination, aPayload, _transmissionMetrics ) );
	}

	/**
	 * Convenience method for {@link #sendMessage(P2PMessage)}.
	 *
	 * @param aDestination The destination of the message.
	 * @param aPayload The payload to be sent.
	 * 
	 * @throws IOException thrown in case I/O problems occurred while accessing
	 *         the mesh.
	 * @throws NoSuchDestinationException thrown in case there is none such
	 *         destination peer.
	 */
	public void sendMessage( int aDestination, Sequence aPayload ) throws IOException, NoSuchDestinationException {
		sendMessage( new SerialP2PMessage( aDestination, aPayload, _transmissionMetrics ) );
	}

	/**
	 * Convenience method for {@link #sendMessage(P2PMessage)}.
	 *
	 * @param <P> The type of the payload in question.
	 * @param aMagicNumber The magic number for tagging this message when being
	 *        transmitted.
	 * @param aDestination The destination of the message.
	 * @param aPayload The payload to be sent.
	 * 
	 * @throws IOException thrown in case I/O problems occurred while accessing
	 *         the mesh.
	 * @throws NoSuchDestinationException thrown in case there is none such
	 *         destination peer.
	 */
	public <P> void sendMessage( int aMagicNumber, int aDestination, P aPayload ) throws IOException, NoSuchDestinationException {
		sendMessage( new SerialP2PMessage( aMagicNumber, aDestination, aPayload, _transmissionMetrics ) );
	}

	/**
	 * Convenience method for {@link #sendMessage(P2PMessage)}.
	 *
	 * @param aMagicNumber The magic number for tagging this message when being
	 *        transmitted.
	 * @param aDestination The destination of the message.
	 * @param aPayload The payload to be sent.
	 * 
	 * @throws IOException thrown in case I/O problems occurred while accessing
	 *         the mesh.
	 * @throws NoSuchDestinationException thrown in case there is none such
	 *         destination peer.
	 */
	public void sendMessage( int aMagicNumber, int aDestination, Segment aPayload ) throws IOException, NoSuchDestinationException {
		sendMessage( new SerialP2PMessage( aMagicNumber, aDestination, aPayload, _transmissionMetrics ) );
	}

	/**
	 * Convenience method for {@link #sendMessage(P2PMessage)}.
	 *
	 * @param aMagicNumber The magic number for tagging this message when being
	 *        transmitted.
	 * @param aDestination The destination of the message.
	 * @param aPayload The payload to be sent.
	 * 
	 * @throws IOException thrown in case I/O problems occurred while accessing
	 *         the mesh.
	 * @throws NoSuchDestinationException thrown in case there is none such
	 *         destination peer.
	 */
	public void sendMessage( int aMagicNumber, int aDestination, Sequence aPayload ) throws IOException, NoSuchDestinationException {
		sendMessage( new SerialP2PMessage( aDestination, aPayload, _transmissionMetrics ) );
	}
}
