// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.p2p.alt.serial;

import static org.refcodes.serial.SerialSugar.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.refcodes.data.IoRetryCount;
import org.refcodes.data.IoSleepLoopTime;
import org.refcodes.exception.TimeoutIOException;
import org.refcodes.exception.Trap;
import org.refcodes.mixin.LocatorAccessor;
import org.refcodes.mixin.StatusAccessor;
import org.refcodes.p2p.HopCountAccessor;
import org.refcodes.p2p.HopsAccessor;
import org.refcodes.p2p.NoSuchDestinationException;
import org.refcodes.p2p.PeerProxy;
import org.refcodes.serial.IntArraySection;
import org.refcodes.serial.IntSegment;
import org.refcodes.serial.Port;
import org.refcodes.serial.PortMetrics;
import org.refcodes.serial.Segment;
import org.refcodes.serial.Sequence;
import org.refcodes.serial.SerialSchema;
import org.refcodes.serial.Transmission.TransmissionMixin;
import org.refcodes.serial.TransmissionException;
import org.refcodes.serial.ext.handshake.HandshakePortController;
import org.refcodes.struct.SimpleTypeMap;
import org.refcodes.struct.SimpleTypeMapImpl;

/**
 * The {@link SerialPeerProxy} implements a {@link PeerProxy} optimized for
 * local area microcontroller wiring (networks) and microcontroller memory
 * layout using serial {@link Port} instances.
 */
public class SerialPeerProxy implements PeerProxy<Integer, SerialP2PHeader, SerialP2PTail, SerialP2PMessage>, SerialPeerRouter {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final Logger LOGGER = Logger.getLogger( SerialPeerProxy.class.getName() );

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final SerialP2PTransmissionMetrics DEFAULT_TRANSMISSION_METRICS = new SerialP2PTransmissionMetrics();

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected HandshakePortController<?> _port;
	private SerialP2PTransmissionMetrics _transmissionMetrics;
	private final Map<Integer, HopCountHeuristic> _locatorToHopCountHeuristics = new WeakHashMap<>();
	private boolean _hasPingHeurisitc = true;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link SerialPeerProxy} using the given {@link Port}. We
	 * assume the port being already opened with the implementation specific
	 * {@link PortMetrics}.
	 * 
	 * @param aPort The {@link Port} use for the {@link SerialPeerProxy}.
	 * @param aPeer The {@link SerialPeer} to which this {@link SerialPeerProxy}
	 *        is being attached.
	 */
	SerialPeerProxy( HandshakePortController<?> aPort, SerialPeer aPeer ) {
		this( aPort, aPeer, DEFAULT_TRANSMISSION_METRICS );
	}

	/**
	 * Constructs a {@link SerialPeerProxy} using the given {@link Port}. We
	 * assume the port being already opened with the implementation specific
	 * {@link PortMetrics}.
	 * 
	 * @param aPort The {@link Port} use for the {@link SerialPeerProxy}.
	 * @param aPeer The {@link SerialPeer} to which this {@link SerialPeerProxy}
	 *        is being attached.
	 * @param aTransmissionMetrics The {@link SerialP2PTransmissionMetrics} to
	 *        use when setting up the peer.
	 */
	SerialPeerProxy( HandshakePortController<?> aPort, SerialPeer aPeer, SerialP2PTransmissionMetrics aTransmissionMetrics ) {
		_transmissionMetrics = aTransmissionMetrics != null ? aTransmissionMetrics : DEFAULT_TRANSMISSION_METRICS;
		aPort.onRequest( new SerialP2PMessage( _transmissionMetrics ), msg -> {
			try {
				aPeer.sendMessage( msg );
				return new SerialP2PMessageResponse( ResponseStatus.OK, _transmissionMetrics );
			}
			catch ( NoSuchDestinationException e ) {
				clearHeuristics();
				LOGGER.log( Level.WARNING, "Unable to send message to <" + msg.getHeader().getDestination() + "> and visited hops <" + Arrays.toString( msg.getTail().getHops() ) + "> as of: " + Trap.asMessage( e ), e );
				return new SerialP2PMessageResponse( ResponseStatus.NO_SUCH_DESTINATION, _transmissionMetrics );
			}
			catch ( IOException e ) {
				clearHeuristics();
				LOGGER.log( Level.WARNING, "Unable to send message to <" + msg.getHeader().getDestination() + "> and visaed hops <" + Arrays.toString( msg.getTail().getHops() ) + "> as of: " + Trap.asMessage( e ), e );
				if ( e instanceof TimeoutIOException ) {
					return new SerialP2PMessageResponse( ResponseStatus.NO_SUCH_DESTINATION, _transmissionMetrics );
				}
				else {
					return new SerialP2PMessageResponse( ResponseStatus.IO_EXCEPTION, _transmissionMetrics );
				}
			}
		} );

		aPort.onRequest( new HopCountRequest( _transmissionMetrics ), request -> {
			try {
				return new HopCountResponse( aPeer.getHopCount( request.getLocator(), request.getHops() ), _transmissionMetrics );
			}
			catch ( IOException e ) {
				clearHeuristics();
				LOGGER.log( Level.WARNING, "Unable to determine hop count for locator <" + request.getLocator() + "> and visited hops <" + Arrays.toString( request.getHops() ) + "> as of: " + Trap.asMessage( e ), e );
				return new HopCountResponse( -1, _transmissionMetrics );
			}
		} );

		aPort.onPing( this::refreshPing );

		_port = aPort;
		try {
			_port.skipAvailableWithin( IoRetryCount.MIN.getValue(), IoSleepLoopTime.MIN.getTimeMillis() );
		}
		catch ( IOException ignore ) {}
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getHopCount( Integer aLocator, Integer[] aHops ) throws IOException {

		if ( !_port.isOpened() ) {
			return -1;
		}

		try {
			probePingHeuristic();
		}
		catch ( IOException e ) {
			LOGGER.log( Level.WARNING, "Cannot determine hop count for locator <" + aLocator + "> (port <" + _port.getAlias() + "> seems not to be connected with a remote peer) as of: " + Trap.asMessage( e ), e );
			return -1;
		}
		final Integer theHopCountHeurisitc = getHopCountHeurisitc( aLocator );
		if ( theHopCountHeurisitc != null ) {
			return theHopCountHeurisitc;
		}
		try {
			final HopCountRequest theRequest = new HopCountRequest( aLocator, aHops, _transmissionMetrics );
			final HopCountResponse theResponse = new HopCountResponse( _transmissionMetrics );
			_port.requestSegment( theRequest, theResponse, _transmissionMetrics.getAcknowledgeMode().isEnabled() );
			final int theHopCount = theResponse.getHopCount();
			putHopCountHeurisitc( aLocator, theHopCount );
			return theHopCount;
		}
		catch ( IOException e ) {
			LOGGER.log( Level.WARNING, "Cannot determine hop count for locator <" + aLocator + "> (port <" + _port.getAlias() + "> seems not to be connected with a remote peer) as of: " + Trap.asMessage( e ), e );
			clearHeuristics();
			return -1;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void sendMessage( SerialP2PMessage aMessage ) throws NoSuchDestinationException, IOException {

		if ( !_port.isOpened() ) {
			throw new IOException( "Unable to send message to <" + aMessage.getHeader().getDestination() + "> and visited hops <" + Arrays.toString( aMessage.getTail().getHops() ) + "> as port <" + _port.getAlias() + "> is in status (<" + _port.getConnectionStatus() + ">) and not(!) in status opened!" );
		}

		probePingHeuristic();
		try {
			final SerialP2PMessageResponse theResponse = new SerialP2PMessageResponse( _transmissionMetrics );
			_port.requestSegment( aMessage, theResponse, _transmissionMetrics.getAcknowledgeMode().isEnabled() );
			refreshHopCountHeurisitc( aMessage.getHeader().getDestination() );
			switch ( theResponse.getStatus() ) {
			case IO_EXCEPTION:
				throw new IOException( "Unable to send message to <" + aMessage.getHeader().getDestination() + "> and visited hops <" + Arrays.toString( aMessage.getTail().getHops() ) + "> over port <" + _port.getAlias() + ">!" );
			case NONE:
				LOGGER.log( Level.WARNING, "Unknown status <" + ResponseStatus.NONE + "> response when sending message to <" + aMessage.getHeader().getDestination() + "> and visited hops <" + Arrays.toString( aMessage.getTail().getHops() ) + "> over port <" + _port.getAlias() + ">!" );
				break;
			case NO_SUCH_DESTINATION:
				throw new NoSuchDestinationException( "Unable to send message to <" + aMessage.getHeader().getDestination() + "> and visited hops <" + Arrays.toString( aMessage.getTail().getHops() ) + "> over port <" + _port.getAlias() + ">!", aMessage.getHeader().getDestination() );
			case OK:
				break;
			default:
				break;
			}
		}
		catch ( NoSuchDestinationException e ) {
			clearHeuristics();
			throw e;
		}
		catch ( IOException e ) {
			clearHeuristics();
			if ( e instanceof TimeoutIOException ) {
				throw new NoSuchDestinationException( "Port <" + _port.getAlias() + "> seems not to be connected with a remote peer! Unable to send message to <" + aMessage.getHeader().getDestination() + "> and visited hops <" + Arrays.toString( aMessage.getTail().getHops() ) + ">!", aMessage.getHeader().getDestination(), e );
			}
			throw e;
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private void clearHeuristics() {
		clearPingHeurisitc();
		clearHopCountHeuristics();
	}

	private void clearPingHeurisitc() {
		_hasPingHeurisitc = false;
	}

	private boolean refreshPing() {
		return _hasPingHeurisitc = true;
	}

	private void probePingHeuristic() throws IOException {
		try {
			if ( !_hasPingHeurisitc ) {
				_port.ping();
				refreshPing();
			}
		}
		catch ( IOException e ) {
			clearHopCountHeuristics();
			throw e;
		}
	}

	private void putHopCountHeurisitc( Integer aLocator, int aHopCount ) {
		// Do not cache hop count = -1 as some time later it could appear |-->
		if ( aHopCount == -1 ) {
			_locatorToHopCountHeuristics.remove( aLocator );
		}
		// Do not cache hop count = -1 as some time later it could appear <--|
		else {
			HopCountHeuristic theHopCountHeuristic = _locatorToHopCountHeuristics.get( aLocator );
			if ( theHopCountHeuristic == null ) {
				theHopCountHeuristic = new HopCountHeuristic( aHopCount );
				_locatorToHopCountHeuristics.put( aLocator, theHopCountHeuristic );
			}
			else {
				theHopCountHeuristic.update( aHopCount );
			}
		}
	}

	private Integer getHopCountHeurisitc( Integer aLocator ) {
		final HopCountHeuristic theHopCountHeurisitcs = _locatorToHopCountHeuristics.get( aLocator );
		if ( theHopCountHeurisitcs != null && theHopCountHeurisitcs.isValid() ) {
			return theHopCountHeurisitcs.hopCount;
		}
		else {
			_locatorToHopCountHeuristics.remove( aLocator );
		}
		return null;
	}

	private void refreshHopCountHeurisitc( Integer aLocator ) {
		final HopCountHeuristic theHopCountHeurisitcs = _locatorToHopCountHeuristics.get( aLocator );
		if ( theHopCountHeurisitcs != null ) {
			theHopCountHeurisitcs.refresh();
		}
	}

	//	private void invalidateHopCountHeuristic( Integer aLocator ) {
	//		_locatorToHopCountHeuristics.remove( aLocator );
	//	}

	private void clearHopCountHeuristics() {
		_locatorToHopCountHeuristics.clear();
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@link Segment} representing a hop count request.
	 */
	static class HopCountRequest implements Segment, TransmissionMixin, LocatorAccessor<Integer>, HopsAccessor<Integer> {

		// /////////////////////////////////////////////////////////////////////
		// STATICS:
		// /////////////////////////////////////////////////////////////////////

		private static final long serialVersionUID = 1L;

		// /////////////////////////////////////////////////////////////////////
		// CONSTANTS:
		// /////////////////////////////////////////////////////////////////////

		private static final SerialP2PTransmissionMetrics DEFAULT_TRANSMISSION_METRICS = new SerialP2PTransmissionMetrics();

		// /////////////////////////////////////////////////////////////////////
		// VARIABLES:
		// /////////////////////////////////////////////////////////////////////

		private Segment _delegatee = null;
		private IntSegment _locatorSegment;
		private IntArraySection _hopsSection;

		// /////////////////////////////////////////////////////////////////////
		// CONSTRUCTORS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Instantiates an empty {@link HopCountRequest}.
		 */
		public HopCountRequest() {
			this( -1, (int[]) null, DEFAULT_TRANSMISSION_METRICS );
		}

		/**
		 * Instantiates an empty {@link HopCountRequest}.
		 * 
		 * @param aTransmissionMetrics The {@link SerialP2PTransmissionMetrics}
		 *        to use.
		 */
		public HopCountRequest( SerialP2PTransmissionMetrics aTransmissionMetrics ) {
			this( -1, (int[]) null, aTransmissionMetrics );
		}

		/**
		 * Instantiates a new {@link HopCountRequest} for the given locator
		 * having passed the provided hops.
		 * 
		 * @param aLocator The locator for which to request the hop count.
		 * 
		 * @param aHops The hops already been visited.
		 */
		public HopCountRequest( int aLocator, Integer[] aHops ) {
			this( aLocator, toUnboxedArray( aHops ), DEFAULT_TRANSMISSION_METRICS );
		}

		/**
		 * Instantiates a new {@link HopCountRequest} for the given locator
		 * having passed the provided hops.
		 * 
		 * @param aLocator The locator for which to request the hop count.
		 * 
		 * @param aHops The hops already been visited.
		 */
		public HopCountRequest( int aLocator, int[] aHops ) {
			this( aLocator, aHops, DEFAULT_TRANSMISSION_METRICS );
		}

		/**
		 * Instantiates a new {@link HopCountRequest} for the given locator
		 * having passed the provided hops.
		 * 
		 * @param aLocator The locator for which to request the hop count.
		 * 
		 * @param aHops The hops already been visited.
		 * 
		 * @param aTransmissionMetrics The {@link SerialP2PTransmissionMetrics}
		 *        to use.
		 */
		public HopCountRequest( int aLocator, Integer[] aHops, SerialP2PTransmissionMetrics aTransmissionMetrics ) {
			this( aLocator, toUnboxedArray( aHops ), aTransmissionMetrics );
		}

		/**
		 * Instantiates a new {@link HopCountRequest} for the given locator
		 * having passed the provided hops.
		 * 
		 * @param aLocator The locator for which to request the hop count.
		 * 
		 * @param aHops The hops already been visited.
		 * 
		 * @param aTransmissionMetrics The {@link SerialP2PTransmissionMetrics}
		 *        to use.
		 */
		public HopCountRequest( int aLocator, int[] aHops, SerialP2PTransmissionMetrics aTransmissionMetrics ) {
			aTransmissionMetrics = aTransmissionMetrics != null ? aTransmissionMetrics : DEFAULT_TRANSMISSION_METRICS;
			// @formatter:off
			_delegatee = segmentComposite(  
				crcSegment(
					segmentComposite(
						assertMagicBytesSegment( aTransmissionMetrics.getHopCountRequestMagicBytes(), aTransmissionMetrics ), _locatorSegment = intSegment( aLocator, aTransmissionMetrics ), allocSegment( _hopsSection = ( aHops != null ? intArraySection( aTransmissionMetrics , aHops) : intArraySection(aTransmissionMetrics )  ) , aTransmissionMetrics ) 
					), 
					aTransmissionMetrics
				)
			);
			// @formatter:on
		}

		// /////////////////////////////////////////////////////////////////////
		// METHODS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Integer getLocator() {
			return _locatorSegment.getPayload();
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Integer[] getHops() {
			return toBoxedArray( _hopsSection.getPayload() );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public int getLength() {
			return _delegatee.getLength();
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Sequence toSequence() {
			return _delegatee.toSequence();
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public String toString() {
			return getClass().getSimpleName() + " [segment=" + _delegatee + "]";
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public SimpleTypeMap toSimpleTypeMap() {
			return _delegatee != null ? _delegatee.toSimpleTypeMap() : new SimpleTypeMapImpl();
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void transmitTo( OutputStream aOutputStream, InputStream aReturnStream ) throws IOException {
			_delegatee.transmitTo( aOutputStream, aReturnStream );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void reset() {
			_delegatee.reset();
			_hopsSection.reset();
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public SerialSchema toSchema() {
			return _delegatee.toSchema();
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public int fromTransmission( Sequence aSequence, int aOffset ) throws TransmissionException {
			return _delegatee.fromTransmission( aSequence, aOffset );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void receiveFrom( InputStream aInputStream, OutputStream aReturnStream ) throws IOException {
			_delegatee.receiveFrom( aInputStream, aReturnStream );

		}

		// /////////////////////////////////////////////////////////////////////
		// HOOKS:
		// /////////////////////////////////////////////////////////////////////

		// /////////////////////////////////////////////////////////////////////
		// HELPER:
		// /////////////////////////////////////////////////////////////////////

		private static Integer[] toBoxedArray( int[] aValues ) {
			final Integer[] theValues = new Integer[aValues.length];
			for ( int i = 0; i < theValues.length; i++ ) {
				theValues[i] = aValues[i];
			}
			return theValues;
		}

		private static int[] toUnboxedArray( Integer[] aValues ) {
			final int[] theValues = new int[aValues.length];
			for ( int i = 0; i < theValues.length; i++ ) {
				theValues[i] = aValues[i];
			}
			return theValues;
		}

		// /////////////////////////////////////////////////////////////////////
		// INNER CLASSES:
		// /////////////////////////////////////////////////////////////////////

	}

	/**
	 * {@link Segment} representing a hop count response.
	 */
	static class HopCountResponse implements Segment, TransmissionMixin, HopCountAccessor {

		// /////////////////////////////////////////////////////////////////////
		// STATICS:
		// /////////////////////////////////////////////////////////////////////

		private static final long serialVersionUID = 1L;

		// /////////////////////////////////////////////////////////////////////
		// CONSTANTS:
		// /////////////////////////////////////////////////////////////////////

		private static final SerialP2PTransmissionMetrics DEFAULT_TRANSMISSION_METRICS = new SerialP2PTransmissionMetrics();

		// /////////////////////////////////////////////////////////////////////
		// VARIABLES:
		// /////////////////////////////////////////////////////////////////////

		private Segment _delegatee = null;
		private IntSegment _hopCountSegment;

		// /////////////////////////////////////////////////////////////////////
		// CONSTRUCTORS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Instantiates a new {@link HopCountResponse} for holding an according
		 * result.
		 */
		public HopCountResponse() {
			this( -1, DEFAULT_TRANSMISSION_METRICS );
		}

		/**
		 * Instantiates a new {@link HopCountResponse} for holding an according
		 * result.
		 * 
		 * @param aTransmissionMetrics The {@link SerialP2PTransmissionMetrics}
		 *        to use.
		 */
		public HopCountResponse( SerialP2PTransmissionMetrics aTransmissionMetrics ) {
			this( -1, aTransmissionMetrics );
		}

		/**
		 * Instantiates a new {@link HopCountResponse} with the given result.
		 *
		 * @param aHopCount The hop count result.
		 */
		public HopCountResponse( int aHopCount ) {
			this( aHopCount, DEFAULT_TRANSMISSION_METRICS );
		}

		/**
		 * Instantiates a new {@link HopCountResponse} with the given result.
		 * 
		 * @param aHopCount The hop count result.
		 * @param aTransmissionMetrics The {@link SerialP2PTransmissionMetrics}
		 *        to use.
		 */
		public HopCountResponse( int aHopCount, SerialP2PTransmissionMetrics aTransmissionMetrics ) {
			// @formatter:off
			_delegatee = segmentComposite(  
				crcSegment(
					segmentComposite(
						assertMagicBytesSegment( aTransmissionMetrics.getHopCountResponseMagicBytes(), aTransmissionMetrics ), _hopCountSegment = intSegment( aHopCount, aTransmissionMetrics ) 
					), 
					aTransmissionMetrics
				)
			);
			// @formatter:on
		}

		// /////////////////////////////////////////////////////////////////////
		// METHODS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * {@inheritDoc}
		 */
		@Override
		public int getHopCount() {
			return _hopCountSegment.getPayload();
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public int getLength() {
			return _delegatee.getLength();
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void reset() {
			_delegatee.reset();
			_hopCountSegment.reset();
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Sequence toSequence() {
			return _delegatee.toSequence();
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public String toString() {
			return getClass().getSimpleName() + " [segment=" + _delegatee + "]";
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public SimpleTypeMap toSimpleTypeMap() {
			return _delegatee != null ? _delegatee.toSimpleTypeMap() : new SimpleTypeMapImpl();
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void transmitTo( OutputStream aOutputStream, InputStream aReturnStream ) throws IOException {
			_delegatee.transmitTo( aOutputStream, aReturnStream );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public SerialSchema toSchema() {
			return _delegatee.toSchema();
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public int fromTransmission( Sequence aSequence, int aOffset ) throws TransmissionException {
			return _delegatee.fromTransmission( aSequence, aOffset );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void receiveFrom( InputStream aInputStream, OutputStream aReturnStream ) throws IOException {
			_delegatee.receiveFrom( aInputStream, aReturnStream );

		}

		// /////////////////////////////////////////////////////////////////////
		// HOOKS:
		// /////////////////////////////////////////////////////////////////////

		// /////////////////////////////////////////////////////////////////////
		// HELPER:
		// /////////////////////////////////////////////////////////////////////

		// /////////////////////////////////////////////////////////////////////
		// INNER CLASSES:
		// /////////////////////////////////////////////////////////////////////

	}

	/**
	 * {@link Segment} representing a hop count request.
	 */
	static class SerialP2PMessageResponse implements Segment, TransmissionMixin, StatusAccessor<ResponseStatus> {

		// /////////////////////////////////////////////////////////////////////
		// STATICS:
		// /////////////////////////////////////////////////////////////////////

		private static final long serialVersionUID = 1L;

		// /////////////////////////////////////////////////////////////////////
		// CONSTANTS:
		// /////////////////////////////////////////////////////////////////////

		// /////////////////////////////////////////////////////////////////////
		// VARIABLES:
		// /////////////////////////////////////////////////////////////////////

		private Segment _delegatee = null;
		private IntSegment _statusSegment;

		// /////////////////////////////////////////////////////////////////////
		// CONSTRUCTORS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Instantiates a new {@link SerialP2PMessageResponse} for holding an
		 * according result.
		 */
		public SerialP2PMessageResponse() {
			this( null, null );
		}

		/**
		 * Instantiates a new {@link SerialP2PMessageResponse} for holding an
		 * according result.
		 * 
		 * @param aStatus The hop count result.
		 */
		public SerialP2PMessageResponse( ResponseStatus aStatus ) {
			this( aStatus, null );
		}

		/**
		 * Instantiates a new {@link SerialP2PMessageResponse} for holding an
		 * according result.
		 * 
		 * @param aTransmissionMetrics The {@link SerialP2PTransmissionMetrics}
		 *        to use.
		 */
		public SerialP2PMessageResponse( SerialP2PTransmissionMetrics aTransmissionMetrics ) {
			this( null, aTransmissionMetrics );
		}

		/**
		 * Instantiates a new {@link SerialP2PMessageResponse} with the given
		 * result.
		 * 
		 * @param aStatus The hop count result.
		 * @param aTransmissionMetrics The {@link SerialP2PTransmissionMetrics}
		 *        to use.
		 */
		public SerialP2PMessageResponse( ResponseStatus aStatus, SerialP2PTransmissionMetrics aTransmissionMetrics ) {
			aTransmissionMetrics = aTransmissionMetrics != null ? aTransmissionMetrics : new SerialP2PTransmissionMetrics();
			// @formatter:off
			_delegatee = segmentComposite(  
				crcSegment(
					segmentComposite(
						assertMagicBytesSegment( aTransmissionMetrics.getP2PMessageResponseMagicBytes(), aTransmissionMetrics ), _statusSegment = intSegment( aStatus != null ? aStatus.getStatusValue() : ResponseStatus.NONE.getStatusValue(), aTransmissionMetrics ) 
					), 
					aTransmissionMetrics
				)
			);
			// @formatter:on
		}

		// /////////////////////////////////////////////////////////////////////
		// METHODS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * {@inheritDoc}
		 */
		@Override
		public ResponseStatus getStatus() {
			return ResponseStatus.toStatus( _statusSegment.getPayload() );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public int getLength() {
			return _delegatee.getLength();
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Sequence toSequence() {
			return _delegatee.toSequence();
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public String toString() {
			return getClass().getSimpleName() + " [segment=" + _delegatee + "]";
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public SimpleTypeMap toSimpleTypeMap() {
			return _delegatee != null ? _delegatee.toSimpleTypeMap() : new SimpleTypeMapImpl();
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void transmitTo( OutputStream aOutputStream, InputStream aReturnStream ) throws IOException {
			_delegatee.transmitTo( aOutputStream, aReturnStream );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void reset() {
			_delegatee.reset();
			_statusSegment.reset();
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public SerialSchema toSchema() {
			return _delegatee.toSchema();
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public int fromTransmission( Sequence aSequence, int aOffset ) throws TransmissionException {
			return _delegatee.fromTransmission( aSequence, aOffset );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void receiveFrom( InputStream aInputStream, OutputStream aReturnStream ) throws IOException {
			_delegatee.receiveFrom( aInputStream, aReturnStream );

		}

		// /////////////////////////////////////////////////////////////////////
		// HOOKS:
		// /////////////////////////////////////////////////////////////////////

		// /////////////////////////////////////////////////////////////////////
		// HELPER:
		// /////////////////////////////////////////////////////////////////////

		// /////////////////////////////////////////////////////////////////////
		// INNER CLASSES:
		// /////////////////////////////////////////////////////////////////////
	}

	class HopCountHeuristic {

		int hopCount;
		long timeStamp;

		HopCountHeuristic( int aHopCount ) {
			hopCount = aHopCount;
			timeStamp = System.currentTimeMillis();
		}

		public void refresh() {
			timeStamp = System.currentTimeMillis();

		}

		public void update( int aHopCount ) {
			hopCount = aHopCount;
			timeStamp = System.currentTimeMillis();
		}

		boolean isValid() {
			return timeStamp != -1 && System.currentTimeMillis() < timeStamp + _transmissionMetrics.getIoHeuristicsTimeToLiveMillis();
		}
	}

	static enum ResponseStatus {

		OK(0), NO_SUCH_DESTINATION(1), IO_EXCEPTION(2), NONE(-1);

		private int _status;

		ResponseStatus( int aStatus ) {
			_status = aStatus;
		}

		public int getStatusValue() {
			return _status;
		}

		public static ResponseStatus toStatus( int aStatusValue ) {
			for ( ResponseStatus eStatus : values() ) {
				if ( eStatus.getStatusValue() == aStatusValue ) {
					return eStatus;
				}
			}
			return NONE;
		}
	}
}
