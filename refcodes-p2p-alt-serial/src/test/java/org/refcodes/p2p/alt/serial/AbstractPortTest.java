// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.p2p.alt.serial;

import java.util.UUID;

import org.refcodes.serial.CrossoverLoopbackPort;
import org.refcodes.serial.LoopbackPort;
import org.refcodes.serial.Port;
import org.refcodes.serial.PortMetrics;

/**
 * Provides base functionality to create {@link Port} instances. Override
 * methods to adjust the tests to different environments such as "loopback",
 * "tty" and so on.
 */
public class AbstractPortTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	private Port<?> _port1 = null;
	private Port<?> _port2 = null;

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	protected boolean hasPorts() {
		return true;
	}

	protected Port<?> getReceiverPort() {
		return getReceiverPort( null );
	}

	protected Port<?> getReceiverPort( PortMetrics aPortMetrics ) {
		if ( _port1 == null ) {
			synchronized ( this ) {
				if ( _port1 == null ) {
					_port1 = new LoopbackPort( "/dev/ttyLoopback" + UUID.randomUUID().toString() );
				}
			}
		}
		return _port1;
	}

	protected Port<?> getTransmitterPort() {
		return getTransmitterPort( null );
	}

	protected Port<?> getTransmitterPort( PortMetrics aPortMetrics ) {
		if ( _port2 == null ) {
			synchronized ( this ) {
				if ( _port2 == null ) {
					_port2 = new CrossoverLoopbackPort( (LoopbackPort) getReceiverPort( aPortMetrics ) );
				}
			}
		}
		return _port2;
	}

	protected void waitForPortCatchUp() {}

	protected void waitLongerForPortCatchUp() {}

	protected void waitBeforePortClose() {}

	protected void waitLongerBeforePortClose() {}

	protected void waitLongestBeforePortClose() {}

	protected void waitForPortCatchUp( long aSleepTimeMillis ) {}
}
