// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.p2p.alt.serial;

import java.io.IOException;

import org.junit.jupiter.api.BeforeEach;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.serial.Port;
import org.refcodes.serial.alt.tty.TtyPort;
import org.refcodes.serial.alt.tty.TtyPortHub;
import org.refcodes.textual.VerboseTextBuilder;

/**
 * Provides base functionality to create {@link Port} instances. Override
 * methods to adjust the tests to different environments such as "loopback",
 * "tty" and so on.
 */
public class AbstractTtyPortTest {

	private static final String[] PORT_TYPES = { "ttl232", "ftdi_sio", "ft232" }; // , "physical" };

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected TtyPort _port1 = null;
	protected TtyPort _port2 = null;
	protected String _alias1 = null;
	protected String _alias2 = null;

	@BeforeEach
	public void beforeEach() throws IOException {
		int i;
		final TtyPortHub thePortHub = new TtyPortHub();
		out: {
			for ( String eCandidate : PORT_TYPES ) {
				i = 0;
				for ( TtyPort ePort : thePortHub.ports() ) {
					if ( ePort.getName().toLowerCase().contains( eCandidate ) ) {
						if ( i == 0 ) {
							if ( SystemProperty.LOG_TESTS.isEnabled() ) {
								System.out.println( "1st port <" + ePort + "> to use ..." );
							}
							_alias1 = ePort.getAlias();
						}
						if ( i == 1 ) {
							if ( SystemProperty.LOG_TESTS.isEnabled() ) {
								System.out.println( "2nd port <" + ePort + "> to use ..." );
							}
							_alias2 = ePort.getAlias();
							break out;
						}
						i++;
					}
				}
			}
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	protected boolean hasPorts() {
		boolean hasPorts = _alias1 != null && _alias2 != null;
		if ( !hasPorts ) {
			System.out.println( "Skipping test, please connect your null modem cable to two serial ports on your box, seeking for two ports these types: " + VerboseTextBuilder.asString( PORT_TYPES ) );
		}
		return hasPorts;
	}

	protected TtyPort getReceiverPort() throws IOException {
		if ( _port1 == null ) {
			synchronized ( this ) {
				if ( _port1 == null ) {
					_port1 = new TtyPortHub().toPort( _alias1 );
				}
			}
		}
		return _port1;
	}

	protected TtyPort getTransmitterPort() throws IOException {
		if ( _port2 == null ) {
			synchronized ( this ) {
				if ( _port2 == null ) {
					_port2 = new TtyPortHub().toPort( _alias2 );
				}
			}
		}
		return _port2;
	}

	/**
	 * TODO 2022-03-24 Investigate why the TTY port needs sleep time to succeed.
	 */
	protected void waitForPortCatchUp() {
		try {
			Thread.sleep( 100 );
		}
		catch ( InterruptedException ignore ) {}
	}

	/**
	 * TODO 2022-03-24 Investigate why the TTY port needs sleep time to succeed.
	 */
	protected void waitLongerForPortCatchUp() {
		try {
			Thread.sleep( 400 );
		}
		catch ( InterruptedException ignore ) {}
	}

	/**
	 * TODO 2022-03-24 Investigate why the TTY port needs sleep time to succeed.
	 */
	protected void waitBeforePortClose() {
		try {
			Thread.sleep( 500 );
		}
		catch ( InterruptedException ignore ) {}
	}

	/**
	 * TODO 2022-03-24 Investigate why the TTY port needs sleep time to succeed.
	 */
	protected void waitLongerBeforePortClose() {
		try {
			Thread.sleep( 600 );
		}
		catch ( InterruptedException ignore ) {}
	}

	/**
	 * TODO 2022-03-24 Investigate why the TTY port needs sleep time to succeed.
	 */
	protected void waitLongestBeforePortClose() {
		try {
			Thread.sleep( 1000 );
		}
		catch ( InterruptedException ignore ) {}
	}

	/**
	 * TODO 2022-03-24 Investigate why the TTY port needs sleep time to succeed.
	 */
	protected void waitForPortCatchUp( long aSleepTimeMillis ) {
		try {
			Thread.sleep( aSleepTimeMillis );
		}
		catch ( InterruptedException ignore ) {}
	}
}
