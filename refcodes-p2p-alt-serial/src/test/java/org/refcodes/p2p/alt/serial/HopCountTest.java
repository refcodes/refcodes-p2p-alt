// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.p2p.alt.serial;

import static org.junit.jupiter.api.Assertions.*;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;

public class HopCountTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testHopCountUnidirectional() throws IOException {
		final SerialPeer thePeerA = new SerialPeer( 1, ( msg, peer ) -> System.out.println( "Received message at <" + peer.getLocator() + "> : " + msg.toString() ) );
		final SerialPeer thePeerB1 = new SerialPeer( 2, ( msg, peer ) -> System.out.println( "Received message at <" + peer.getLocator() + "> : " + msg.toString() ) );
		final SerialPeer thePeerB2 = new SerialPeer( 3, ( msg, peer ) -> System.out.println( "Received message at <" + peer.getLocator() + "> : " + msg.toString() ) );
		final SerialPeer thePeerC = new SerialPeer( 4, ( msg, peer ) -> System.out.println( "Received message at <" + peer.getLocator() + "> : " + msg.toString() ) );
		final SerialPeer thePeerD = new SerialPeer( 5, ( msg, peer ) -> System.out.println( "Received message at <" + peer.getLocator() + "> : " + msg.toString() ) );
		final SerialPeer thePeerE = new SerialPeer( 6, ( msg, peer ) -> System.out.println( "Received message at <" + peer.getLocator() + "> : " + msg.toString() ) );
		final List<SerialPeer> thePeers = Arrays.asList( thePeerA, thePeerB1, thePeerB2, thePeerC, thePeerD, thePeerE );
		thePeerA.addPeerRouter( thePeerB1 );
		thePeerB1.addPeerRouter( thePeerB2 );
		thePeerB2.addPeerRouter( thePeerC );
		thePeerA.addPeerRouter( thePeerC );
		thePeerC.addPeerRouter( thePeerD );
		thePeerD.addPeerRouter( thePeerE );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Unidirectional:" );
			for ( SerialPeer fromPeer : thePeers ) {
				for ( SerialPeer toPeer : thePeers ) {
					System.out.println( fromPeer.getLocator() + " --(" + fromPeer.getHopCount( toPeer.getLocator() ) + ")--> " + toPeer.getLocator() );
				}
			}
		}
		assertEquals( 0, thePeerA.getHopCount( 1 ) );
		assertEquals( 1, thePeerA.getHopCount( 2 ) );
		assertEquals( 2, thePeerA.getHopCount( 3 ) );
		assertEquals( 1, thePeerA.getHopCount( 4 ) );
		assertEquals( 2, thePeerA.getHopCount( 5 ) );
		assertEquals( 3, thePeerA.getHopCount( 6 ) );
		assertEquals( -1, thePeerB1.getHopCount( 1 ) );
		assertEquals( 0, thePeerB1.getHopCount( 2 ) );
		assertEquals( 1, thePeerB1.getHopCount( 3 ) );
		assertEquals( 2, thePeerB1.getHopCount( 4 ) );
		assertEquals( 3, thePeerB1.getHopCount( 5 ) );
		assertEquals( 4, thePeerB1.getHopCount( 6 ) );
		assertEquals( -1, thePeerB2.getHopCount( 1 ) );
		assertEquals( -1, thePeerB2.getHopCount( 2 ) );
		assertEquals( 0, thePeerB2.getHopCount( 3 ) );
		assertEquals( 1, thePeerB2.getHopCount( 4 ) );
		assertEquals( 2, thePeerB2.getHopCount( 5 ) );
		assertEquals( 3, thePeerB2.getHopCount( 6 ) );
		assertEquals( -1, thePeerC.getHopCount( 1 ) );
		assertEquals( -1, thePeerC.getHopCount( 2 ) );
		assertEquals( -1, thePeerC.getHopCount( 3 ) );
		assertEquals( 0, thePeerC.getHopCount( 4 ) );
		assertEquals( 1, thePeerC.getHopCount( 5 ) );
		assertEquals( 2, thePeerC.getHopCount( 6 ) );
		assertEquals( -1, thePeerD.getHopCount( 1 ) );
		assertEquals( -1, thePeerD.getHopCount( 2 ) );
		assertEquals( -1, thePeerD.getHopCount( 3 ) );
		assertEquals( -1, thePeerD.getHopCount( 4 ) );
		assertEquals( 0, thePeerD.getHopCount( 5 ) );
		assertEquals( 1, thePeerD.getHopCount( 6 ) );
		assertEquals( -1, thePeerE.getHopCount( 1 ) );
		assertEquals( -1, thePeerE.getHopCount( 2 ) );
		assertEquals( -1, thePeerE.getHopCount( 3 ) );
		assertEquals( -1, thePeerE.getHopCount( 4 ) );
		assertEquals( -1, thePeerE.getHopCount( 5 ) );
		assertEquals( 0, thePeerE.getHopCount( 6 ) );
	}

	@Test
	public void testHopCountBidirectional() throws IOException {
		final SerialPeer thePeerA = new SerialPeer( 1, ( msg, peer ) -> System.out.println( "Received message at <" + peer.getLocator() + "> : " + msg.toString() ) );
		final SerialPeer thePeerB1 = new SerialPeer( 2, ( msg, peer ) -> System.out.println( "Received message at <" + peer.getLocator() + "> : " + msg.toString() ) );
		final SerialPeer thePeerB2 = new SerialPeer( 3, ( msg, peer ) -> System.out.println( "Received message at <" + peer.getLocator() + "> : " + msg.toString() ) );
		final SerialPeer thePeerC = new SerialPeer( 4, ( msg, peer ) -> System.out.println( "Received message at <" + peer.getLocator() + "> : " + msg.toString() ) );
		final SerialPeer thePeerD = new SerialPeer( 5, ( msg, peer ) -> System.out.println( "Received message at <" + peer.getLocator() + "> : " + msg.toString() ) );
		final SerialPeer thePeerE = new SerialPeer( 6, ( msg, peer ) -> System.out.println( "Received message at <" + peer.getLocator() + "> : " + msg.toString() ) );
		final List<SerialPeer> thePeers = Arrays.asList( thePeerA, thePeerB1, thePeerB2, thePeerC, thePeerD, thePeerE );
		thePeerA.addPeerRouter( thePeerB1 );
		thePeerB1.addPeerRouter( thePeerA );
		thePeerB1.addPeerRouter( thePeerB2 );
		thePeerB2.addPeerRouter( thePeerB1 );
		thePeerB2.addPeerRouter( thePeerC );
		thePeerC.addPeerRouter( thePeerB2 );
		thePeerA.addPeerRouter( thePeerC );
		thePeerC.addPeerRouter( thePeerA );
		thePeerC.addPeerRouter( thePeerD );
		thePeerD.addPeerRouter( thePeerC );
		thePeerD.addPeerRouter( thePeerE );
		thePeerE.addPeerRouter( thePeerD );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Bidirectional:" );
			for ( SerialPeer fromPeer : thePeers ) {
				for ( SerialPeer toPeer : thePeers ) {
					System.out.println( fromPeer.getLocator() + " --(" + fromPeer.getHopCount( toPeer.getLocator() ) + ")--> " + toPeer.getLocator() );
				}
			}
		}
		assertEquals( 0, thePeerA.getHopCount( 1 ) );
		assertEquals( 1, thePeerA.getHopCount( 2 ) );
		assertEquals( 2, thePeerA.getHopCount( 3 ) );
		assertEquals( 1, thePeerA.getHopCount( 4 ) );
		assertEquals( 2, thePeerA.getHopCount( 5 ) );
		assertEquals( 3, thePeerA.getHopCount( 6 ) );
		assertEquals( 1, thePeerB1.getHopCount( 1 ) );
		assertEquals( 0, thePeerB1.getHopCount( 2 ) );
		assertEquals( 1, thePeerB1.getHopCount( 3 ) );
		assertEquals( 2, thePeerB1.getHopCount( 4 ) );
		assertEquals( 3, thePeerB1.getHopCount( 5 ) );
		assertEquals( 4, thePeerB1.getHopCount( 6 ) );
		assertEquals( 2, thePeerB2.getHopCount( 1 ) );
		assertEquals( 1, thePeerB2.getHopCount( 2 ) );
		assertEquals( 0, thePeerB2.getHopCount( 3 ) );
		assertEquals( 1, thePeerB2.getHopCount( 4 ) );
		assertEquals( 2, thePeerB2.getHopCount( 5 ) );
		assertEquals( 3, thePeerB2.getHopCount( 6 ) );
		assertEquals( 1, thePeerC.getHopCount( 1 ) );
		assertEquals( 2, thePeerC.getHopCount( 2 ) );
		assertEquals( 1, thePeerC.getHopCount( 3 ) );
		assertEquals( 0, thePeerC.getHopCount( 4 ) );
		assertEquals( 1, thePeerC.getHopCount( 5 ) );
		assertEquals( 2, thePeerC.getHopCount( 6 ) );
		assertEquals( 2, thePeerD.getHopCount( 1 ) );
		assertEquals( 3, thePeerD.getHopCount( 2 ) );
		assertEquals( 2, thePeerD.getHopCount( 3 ) );
		assertEquals( 1, thePeerD.getHopCount( 4 ) );
		assertEquals( 0, thePeerD.getHopCount( 5 ) );
		assertEquals( 1, thePeerD.getHopCount( 6 ) );
		assertEquals( 3, thePeerE.getHopCount( 1 ) );
		assertEquals( 4, thePeerE.getHopCount( 2 ) );
		assertEquals( 3, thePeerE.getHopCount( 3 ) );
		assertEquals( 2, thePeerE.getHopCount( 4 ) );
		assertEquals( 1, thePeerE.getHopCount( 5 ) );
		assertEquals( 0, thePeerE.getHopCount( 6 ) );
	}
}
