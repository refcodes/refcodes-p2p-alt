// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.p2p.alt.serial;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.refcodes.exception.UnmarshalException;
import org.refcodes.p2p.Peer;
import org.refcodes.p2p.alt.serial.SerialP2PMessageTest.Payload.Sensor;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.serial.Sequence;
import org.refcodes.serial.TransmissionException;

/**
 * Tests the {@link Peer} type.
 */
public class SerialP2PMessageTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testSerialP2PMessage() throws TransmissionException, UnmarshalException {
		final Payload thePayloadA = new Payload( Sensor.LEFT, "SPACEX01" );
		final SerialP2PMessage theMessageA = new SerialP2PMessage( 99, 0, thePayloadA );
		final Sequence theSequence = theMessageA.toSequence();
		final SerialP2PMessage theMessageB = new SerialP2PMessage( 99, 0, new Payload() );
		theMessageB.fromTransmission( theSequence );
		final Payload thePayloadB = theMessageB.getPayload( Payload.class );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theMessageA.getHeader().getMagicNumber() + " -?-> " + theMessageB.getHeader().getMagicNumber() );
			System.out.println( thePayloadA.getName() + " -?-> " + thePayloadB.getName() );
			System.out.println( thePayloadA.getSensor() + " -?-> " + thePayloadB.getSensor() );
		}
		assertEquals( (Integer) 99, theMessageB.getHeader().getMagicNumber() );
		assertEquals( "SPACEX01", thePayloadB.getName() );
		assertEquals( Sensor.LEFT, thePayloadB.getSensor() );
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	public static class Payload {
		public enum Sensor {
			MAIN, LEFT, RIGHT
		}

		Sensor sensor;
		String name;

		Payload( Sensor sensor, String name ) {
			this.sensor = sensor;
			this.name = name;
		}

		public Payload() {}

		public Sensor getSensor() {
			return sensor;
		}

		public void setSensor( Sensor sensor ) {
			this.sensor = sensor;
		}

		public String getName() {
			return name;
		}

		public void setName( String name ) {
			this.name = name;
		}
	}
}
