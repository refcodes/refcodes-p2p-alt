// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.p2p.alt.serial;

import static org.junit.jupiter.api.Assertions.*;
import static org.refcodes.serial.SerialSugar.*;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.graphstream.graph.Graph;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.ui.view.Viewer;
import org.graphstream.ui.view.Viewer.CloseFramePolicy;
import org.junit.jupiter.api.Test;
import org.refcodes.p2p.NoSuchDestinationException;
import org.refcodes.runtime.SystemProperty;

public class SerialPeerTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////
	/**
	 * <code>
	 * 	         /‾‾‾‾‾‾‾‾‾\
	 * 	1 - 2 - 3   4 - 5 - 6 - 7 - 8
	 * 	\__________/
	 * 	</code>
	 */
	@Test
	public void testSendMessageUnidirectional() throws NoSuchDestinationException, IOException {
		final SerialPeer thePeer1 = new SerialPeer( 1, ( msg, peer ) -> System.out.println( "Received message at <" + peer.getLocator() + "> : " + msg.toString() ) );
		final SerialPeer thePeer2 = new SerialPeer( 2, ( msg, peer ) -> System.out.println( "Received message at <" + peer.getLocator() + "> : " + msg.toString() ) );
		final SerialPeer thePeer3 = new SerialPeer( 3, ( msg, peer ) -> System.out.println( "Received message at <" + peer.getLocator() + "> : " + msg.toString() ) );
		final SerialPeer thePeer4 = new SerialPeer( 4, ( msg, peer ) -> System.out.println( "Received message at <" + peer.getLocator() + "> : " + msg.toString() ) );
		final SerialPeer thePeer5 = new SerialPeer( 5, ( msg, peer ) -> System.out.println( "Received message at <" + peer.getLocator() + "> : " + msg.toString() ) );
		final SerialPeer thePeer6 = new SerialPeer( 6, ( msg, peer ) -> System.out.println( "Received message at <" + peer.getLocator() + "> : " + msg.toString() ) );
		final SerialPeer thePeer7 = new SerialPeer( 7, ( msg, peer ) -> System.out.println( "Received message at <" + peer.getLocator() + "> : " + msg.toString() ) );
		final SerialPeer thePeer8 = new SerialPeer( 8, ( msg, peer ) -> System.out.println( "Received message at <" + peer.getLocator() + "> : " + msg.toString() ) );
		final List<SerialPeer> thePeers = Arrays.asList( thePeer1, thePeer2, thePeer3, thePeer4, thePeer5, thePeer6, thePeer7, thePeer8 );
		thePeer1.addPeerRouter( thePeer2 );
		thePeer2.addPeerRouter( thePeer3 );
		thePeer3.addPeerRouter( thePeer6 );
		thePeer1.addPeerRouter( thePeer4 );
		thePeer4.addPeerRouter( thePeer5 );
		thePeer5.addPeerRouter( thePeer6 );
		thePeer6.addPeerRouter( thePeer7 );
		thePeer7.addPeerRouter( thePeer8 );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			viewPeers( thePeers );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Unidirectional:" );
			for ( SerialPeer fromPeer : thePeers ) {
				for ( SerialPeer toPeer : thePeers ) {
					System.out.println( fromPeer.getLocator() + " --(" + fromPeer.getHopCount( toPeer.getLocator() ) + ")--> " + toPeer.getLocator() );
				}
			}
		}
		thePeer1.sendMessage( 8, stringSegment( "Hallo Peer <8>!" ) );
	}

	/**
	 * <code>
	 * 	/‾‾‾‾‾‾‾‾‾‾‾\         
	 * 	1 - 2 - 3 - 4 - 5 - 6 
	 * </code>
	 */
	@Test
	public void testHopCountBidirectional() throws NoSuchDestinationException, IOException {
		final SerialPeer thePeer1 = new SerialPeer( 1, ( msg, peer ) -> System.out.println( "Received message at <" + peer.getLocator() + "> : " + msg.toString() ) );
		final SerialPeer thePeer2 = new SerialPeer( 2, ( msg, peer ) -> System.out.println( "Received message at <" + peer.getLocator() + "> : " + msg.toString() ) );
		final SerialPeer thePeer3 = new SerialPeer( 3, ( msg, peer ) -> System.out.println( "Received message at <" + peer.getLocator() + "> : " + msg.toString() ) );
		final SerialPeer thePeer4 = new SerialPeer( 4, ( msg, peer ) -> System.out.println( "Received message at <" + peer.getLocator() + "> : " + msg.toString() ) );
		final SerialPeer thePeer5 = new SerialPeer( 5, ( msg, peer ) -> System.out.println( "Received message at <" + peer.getLocator() + "> : " + msg.toString() ) );
		final SerialPeer thePeer6 = new SerialPeer( 6, ( msg, peer ) -> System.out.println( "Received message at <" + peer.getLocator() + "> : " + msg.toString() ) );
		final List<SerialPeer> thePeers = Arrays.asList( thePeer1, thePeer2, thePeer3, thePeer4, thePeer5, thePeer6 );
		thePeer1.addPeerRouter( thePeer2 );
		thePeer2.addPeerRouter( thePeer1 );
		thePeer2.addPeerRouter( thePeer3 );
		thePeer3.addPeerRouter( thePeer2 );
		thePeer3.addPeerRouter( thePeer4 );
		thePeer4.addPeerRouter( thePeer3 );
		thePeer1.addPeerRouter( thePeer4 );
		thePeer4.addPeerRouter( thePeer1 );
		thePeer4.addPeerRouter( thePeer5 );
		thePeer5.addPeerRouter( thePeer4 );
		thePeer5.addPeerRouter( thePeer6 );
		thePeer6.addPeerRouter( thePeer5 );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			viewPeers( thePeers );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Bidirectional:" );
			for ( SerialPeer fromPeer : thePeers ) {
				for ( SerialPeer toPeer : thePeers ) {
					System.out.println( fromPeer.getLocator() + " --(" + fromPeer.getHopCount( toPeer.getLocator() ) + ")--> " + toPeer.getLocator() );
				}
			}
		}
		thePeer1.sendMessage( 6, "Hallo Peer <6>!" );
		assertEquals( 0, thePeer1.getHopCount( 1 ) );
		assertEquals( 1, thePeer1.getHopCount( 2 ) );
		assertEquals( 2, thePeer1.getHopCount( 3 ) );
		assertEquals( 1, thePeer1.getHopCount( 4 ) );
		assertEquals( 2, thePeer1.getHopCount( 5 ) );
		assertEquals( 3, thePeer1.getHopCount( 6 ) );
		assertEquals( 1, thePeer2.getHopCount( 1 ) );
		assertEquals( 0, thePeer2.getHopCount( 2 ) );
		assertEquals( 1, thePeer2.getHopCount( 3 ) );
		assertEquals( 2, thePeer2.getHopCount( 4 ) );
		assertEquals( 3, thePeer2.getHopCount( 5 ) );
		assertEquals( 4, thePeer2.getHopCount( 6 ) );
		assertEquals( 2, thePeer3.getHopCount( 1 ) );
		assertEquals( 1, thePeer3.getHopCount( 2 ) );
		assertEquals( 0, thePeer3.getHopCount( 3 ) );
		assertEquals( 1, thePeer3.getHopCount( 4 ) );
		assertEquals( 2, thePeer3.getHopCount( 5 ) );
		assertEquals( 3, thePeer3.getHopCount( 6 ) );
		assertEquals( 1, thePeer4.getHopCount( 1 ) );
		assertEquals( 2, thePeer4.getHopCount( 2 ) );
		assertEquals( 1, thePeer4.getHopCount( 3 ) );
		assertEquals( 0, thePeer4.getHopCount( 4 ) );
		assertEquals( 1, thePeer4.getHopCount( 5 ) );
		assertEquals( 2, thePeer4.getHopCount( 6 ) );
		assertEquals( 2, thePeer5.getHopCount( 1 ) );
		assertEquals( 3, thePeer5.getHopCount( 2 ) );
		assertEquals( 2, thePeer5.getHopCount( 3 ) );
		assertEquals( 1, thePeer5.getHopCount( 4 ) );
		assertEquals( 0, thePeer5.getHopCount( 5 ) );
		assertEquals( 1, thePeer5.getHopCount( 6 ) );
		assertEquals( 3, thePeer6.getHopCount( 1 ) );
		assertEquals( 4, thePeer6.getHopCount( 2 ) );
		assertEquals( 3, thePeer6.getHopCount( 3 ) );
		assertEquals( 2, thePeer6.getHopCount( 4 ) );
		assertEquals( 1, thePeer6.getHopCount( 5 ) );
		assertEquals( 0, thePeer6.getHopCount( 6 ) );
		try {
			thePeer1.sendMessage( new SerialP2PMessage( 7, stringSegment( "Hallo Peer <7>?" ) ) );
			fail( "Expected a <" + NoSuchDestinationException.class.getSimpleName() + ">!" );
		}
		catch ( NoSuchDestinationException expected ) {}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	boolean isKeyPressed = false;

	protected void viewPeers( List<SerialPeer> aPeers ) {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			final Graph theGraph = new SingleGraph( "Peers" );
			theGraph.setStrict( false );
			for ( SerialPeer ePeer : aPeers ) {
				theGraph.addNode( Integer.toString( ePeer.getLocator() ) ).addAttribute( "ui.label", ePeer.getLocator() );
				System.out.println( ePeer.getLocator() );
			}
			for ( SerialPeer ePeer : aPeers ) {
				for ( SerialPeerRouter eToPeer : ePeer.peerRouters() ) {
					final int eToLocator = eToPeer instanceof SerialPeer ? ( (SerialPeer) eToPeer ).getLocator() : eToPeer.hashCode();
					theGraph.addEdge( ePeer.getLocator() + ":" + eToLocator, Integer.toString( ePeer.getLocator() ), Integer.toString( eToLocator ) );
					System.out.println( ePeer.getLocator() + ":" + eToLocator + ", " + ePeer.getLocator() + ", " + eToLocator );
					//	theGraph.addEdge( ePeer.getLocator() + ":" + eToPeer.getLocator(), "" + ePeer.getLocator(), "" + eToPeer.getLocator() );
					//	System.out.println( ePeer.getLocator() + ":" + eToPeer.getLocator() + ", " + ePeer.getLocator() + ", " + eToPeer.getLocator() );
				}
			}
			final Viewer theViewer = theGraph.display();
			theViewer.getDefaultView().addKeyListener( new KeyListener() {
				@Override
				public void keyTyped( KeyEvent e ) {
					isKeyPressed = true;
				}

				@Override
				public void keyReleased( KeyEvent e ) {
					isKeyPressed = true;
				}

				@Override
				public void keyPressed( KeyEvent e ) {
					isKeyPressed = true;
				}
			} );
			theViewer.getDefaultView().addMouseListener( new MouseListener() {
				@Override
				public void mouseReleased( MouseEvent e ) {
					isKeyPressed = true;
				}

				@Override
				public void mousePressed( MouseEvent e ) {
					isKeyPressed = true;
				}

				@Override
				public void mouseExited( MouseEvent arg0 ) {}

				@Override
				public void mouseEntered( MouseEvent arg0 ) {}

				@Override
				public void mouseClicked( MouseEvent arg0 ) {
					isKeyPressed = true;
				}
			} );
			theViewer.setCloseFramePolicy( CloseFramePolicy.EXIT );
			while ( !isKeyPressed ) {
				try {
					Thread.sleep( 100 );
				}
				catch ( InterruptedException ignore ) {}
			}
		}
	}
}
