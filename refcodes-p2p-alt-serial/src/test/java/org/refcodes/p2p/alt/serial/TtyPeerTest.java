// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.p2p.alt.serial;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.refcodes.exception.Trap;
import org.refcodes.exception.UnmarshalException;
import org.refcodes.numerical.CrcStandard;
import org.refcodes.numerical.Endianess;
import org.refcodes.p2p.NoSuchDestinationException;
import org.refcodes.p2p.alt.serial.SerialPeerProxy.HopCountRequest;
import org.refcodes.p2p.alt.serial.SerialPeerProxy.HopCountResponse;
import org.refcodes.p2p.alt.serial.SerialPeerProxy.ResponseStatus;
import org.refcodes.p2p.alt.serial.SerialPeerProxy.SerialP2PMessageResponse;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.serial.Port;
import org.refcodes.serial.Sequence;
import org.refcodes.serial.alt.tty.BaudRate;
import org.refcodes.serial.alt.tty.Handshake;
import org.refcodes.serial.alt.tty.Parity;
import org.refcodes.serial.alt.tty.StopBits;
import org.refcodes.serial.alt.tty.TtyPortHubSingleton;
import org.refcodes.serial.alt.tty.TtyPortMetrics;
import org.refcodes.serial.ext.handshake.HandshakePortController;
import org.refcodes.textual.VerboseTextBuilder;

public class TtyPeerTest extends AbstractTtyPortTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	@Disabled
	public void demoCode() throws IOException, NoSuchDestinationException {
		// @formatter:off
		final var txMetrics = SerialP2PTransmissionMetrics.builder().
			withEndianess( Endianess.LITTLE ).
			withLengthWidth( 4 ).
			withCrcAlgorithm( CrcStandard.CRC_16_X25 ).
			withTransmissionTimeoutMillis( 5000 ).
			withEncoding( StandardCharsets.US_ASCII ).
			build(); 
		// @formatter:on
		// @formatter:off
		final var portMetrics = TtyPortMetrics.builder().
			withBaudRate( BaudRate.BPS_960000_FTDI ).
			withDataBits( 8 ).
			withHandshake( Handshake.AUTO ).
			withParity( Parity.ODD ).
			withStopBits( StopBits.ONE ).
			build(); 
		// @formatter:on
		// @formatter:off
		final var port = TtyPortHubSingleton.getInstance().toPort( "/dev/ttyUSB0" ).withOpen( portMetrics );
		
		final var peer = new SerialPeer( 99, (m, p) -> {
			try {
				System.out.println(p.getLocator() + ": " + m.getPayload( String.class ));
			}
			catch ( UnmarshalException e ) {
				System.err.println( "Who sent THIS(!) message!?!" );
				e.printStackTrace();
			}
		}, txMetrics, port );
		
		peer.sendMessage( 93, "Hello World!" );
		
		// @formatter:on
	}

	@Test
	public void testSerialP2PMessage1() throws IOException, UnmarshalException {
		final SerialP2PTransmissionMetrics theMetrics = new SerialP2PTransmissionMetrics();
		final SerialP2PMessage theSource = new SerialP2PMessage( 100, "Hallo Welt!", theMetrics );
		theSource.getTail().appendHop( 1 );
		theSource.getTail().appendHop( 99 );
		final Sequence theSourceSequence = theSource.toSequence();
		final SerialP2PMessage theResult = new SerialP2PMessage( theMetrics );
		theResult.fromTransmission( theSourceSequence );
		final Sequence theResultSequence = theResult.toSequence();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Sequence (Source): " + VerboseTextBuilder.asString( theSourceSequence.toBytes() ) );
			System.out.println( "Sequence (Result): " + VerboseTextBuilder.asString( theResultSequence.toBytes() ) );
			System.out.println( "AbstractSchema (Source): " + theSource.toSchema() );
			System.out.println( "AbstractSchema (Result): " + theResult.toSchema() );
			System.out.println( "Source (Source) : " + theSource.getTail().getSource() );
			System.out.println( "Source (Result) : " + theResult.getTail().getSource() );
			final Integer theDestination1 = theSource.getHeader().getDestination();
			final Integer theDestination2 = theResult.getHeader().getDestination();
			System.out.println( "Destination (Source) : " + theDestination1 );
			System.out.println( "Destination (Result) : " + theDestination2 );
			final String thePayload1 = theSource.getPayload( String.class );
			final String thePayload2 = theResult.getPayload( String.class );
			System.out.println( "Payload (Source): " + thePayload1 );
			System.out.println( "Payload (Result): " + thePayload2 );
			System.out.println( "Hops (Source): " + VerboseTextBuilder.asString( theSource.getTail().getHops() ) );
			System.out.println( "Hops (Result): " + VerboseTextBuilder.asString( theResult.getTail().getHops() ) );
		}
		assertEquals( 1, theSource.getTail().getSource() );
		assertEquals( 1, theResult.getTail().getSource() );
		assertEquals( 100, theSource.getHeader().getDestination() );
		assertEquals( 100, theResult.getHeader().getDestination() );
		assertArrayEquals( new Integer[] { 1, 99 }, theSource.getTail().getHops() );
		assertArrayEquals( new Integer[] { 1, 99 }, theResult.getTail().getHops() );
		assertEquals( "Hallo Welt!", theSource.getPayload( String.class ) );
		assertEquals( "Hallo Welt!", theResult.getPayload( String.class ) );
		theResult.getTail().appendHop( 200 );
		theSource.fromTransmission( theResult.toSequence() );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Hops (Source): " + VerboseTextBuilder.asString( theSource.getTail().getHops() ) );
			System.out.println( "Hops (Result): " + VerboseTextBuilder.asString( theResult.getTail().getHops() ) );
		}
	}

	@Test
	public void testSerialP2PMessage2() throws IOException {
		if ( hasPorts() ) {
			final Port<?> theTransmitPort = getTransmitterPort().withOpen();
			final Port<?> theReceiverPort = getReceiverPort().withOpen();
			final HandshakePortController<?> theHandshakeTransmitPort = new HandshakePortController<>( theTransmitPort );
			final HandshakePortController<?> theHandshakeReceivePort = new HandshakePortController<>( theReceiverPort );
			final SerialP2PMessage theMessage = new SerialP2PMessage( 1, "Hallo Welt!" );
			theMessage.getTail().appendHop( 99 );
			final SerialP2PMessageResponse theMessageResponse = new SerialP2PMessageResponse();
			final HopCountRequest theHopCountRequest = new HopCountRequest( 1, new int[] { 2, 3, 4 } );
			final HopCountResponse theHopCountResponse = new HopCountResponse();
			theHandshakeReceivePort.onRequest( new HopCountRequest(), req -> {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Received hop count request: " + req.getLocator() + ", " + Arrays.toString( req.getHops() ) );
				}
				return new HopCountResponse( 3 );
			} );
			theHandshakeReceivePort.onRequest( new SerialP2PMessage(), req -> {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					try {
						System.out.println( "Received P2P request: " + req.getPayload( String.class ) );
					}
					catch ( UnmarshalException e ) {
						System.out.println( e.toMessage() );
						e.printStackTrace();
					}
				}
				return new SerialP2PMessageResponse( ResponseStatus.OK );
			} );
			System.out.println( theHopCountRequest.toSequence().toHexString() );
			theHandshakeTransmitPort.requestSegment( theHopCountRequest, theHopCountResponse );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Received P2P response: " + theHopCountResponse.getHopCount() );
			}
			try {
				theHandshakeTransmitPort.requestSegment( theMessage, theMessageResponse );
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Received P2P response: " + theMessageResponse.getStatus() );
				}
			}
			catch ( Exception e ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( Trap.asMessage( e ) );
					e.printStackTrace();
				}
			}
			waitLongestBeforePortClose();
			theHandshakeTransmitPort.close();
			theHandshakeReceivePort.close();
		}
	}

	@Test
	public void testSerialP2PMessage3() throws IOException {
		if ( hasPorts() ) {
			final Port<?> theTransmitPort = getTransmitterPort().withOpen();
			final Port<?> theReceiverPort = getReceiverPort().withOpen();
			final HandshakePortController<?> theHandshakeTransmitPort = new HandshakePortController<>( theTransmitPort );
			final HandshakePortController<?> theHandshakeReceivePort = new HandshakePortController<>( theReceiverPort );
			final SerialP2PMessage theMessage = new SerialP2PMessage( 1, "Hallo Welt!" );
			final SerialP2PMessageResponse theResponse = new SerialP2PMessageResponse();
			theHandshakeReceivePort.onRequest( new SerialP2PMessage(), req -> {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					try {
						System.out.println( "Received request: " + req.getPayload( String.class ) );
					}
					catch ( UnmarshalException e ) {
						System.out.println( e.toMessage() );
						e.printStackTrace();
					}
				}
				return new SerialP2PMessageResponse( ResponseStatus.OK );
			} );
			theHandshakeTransmitPort.requestSegment( theMessage, theResponse );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Received response: " + theResponse.getStatus() );
			}
			waitLongestBeforePortClose();
			theHandshakeTransmitPort.close();
			theHandshakeReceivePort.close();
		}
	}
}
